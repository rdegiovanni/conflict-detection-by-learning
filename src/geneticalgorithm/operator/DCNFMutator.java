package geneticalgorithm.operator;

import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import org.jgap.BaseGeneticOperator;
import org.jgap.Configuration;
import org.jgap.Gene;
import org.jgap.IGeneticOperatorConstraint;
import org.jgap.InvalidConfigurationException;
import org.jgap.Population;
import org.jgap.RandomGenerator;
import org.jgap.data.config.Configurable;

import geneticalgorithm.chromosome.DCNFChromosome;
import geneticalgorithm.chromosome.DCNFOperandGene;
import gov.nasa.ltl.trans.Formula;

/**
 * Mutation operator for DCNFChromosomes
 */
public class DCNFMutator extends BaseGeneticOperator implements Configurable {

	public DCNFMutator(Configuration a_configuration)
			throws InvalidConfigurationException {
		super(a_configuration);
	}
	
	@Override
	public void operate(Population a_population, List a_candidateChromosomes) {
		if (a_population == null || a_candidateChromosomes == null) {
		      // Population or candidate chromosomes list empty:
		      // nothing to do.
		      // -----------------------------------------------
		      return;
		 }
		
		 int size = a_population.size();
		 IGeneticOperatorConstraint constraint = getConfiguration().getJGAPFactory().getGeneticOperatorConstraint();

		 RandomGenerator generator = getConfiguration().getRandomGenerator();
		 for (int i = 0; i < size; i++) {
			 DCNFChromosome chrom = (DCNFChromosome)a_population.getChromosome(i);
		     Gene[] genes = chrom.getGenes();
		     DCNFChromosome copyOfChromosome = null;
		     // For each Chromosome in the population...
		     		      
		     // Get the positions with active genes (genes which expression is not true)
		     int activeGenesPositions = getActivePositions(genes);
		     boolean mutate=false;
		     int m_mutationRate = activeGenesPositions; //TODO: check with no standard mutation rate
		     for (int j = 0; j < activeGenesPositions; j++) {
		    	 mutate = (generator.nextInt(m_mutationRate) == 0);
		    	 if (mutate) {
		    		 if (constraint != null) {
		    			 List v = new Vector();
		    			 v.add(chrom);
		    			 if (!constraint.isValid(a_population, v, this)) {
		    				 continue;
		    			 }
		    		 }
		    		 
		    		 if (copyOfChromosome == null) {
		    			 // ...take a copy of it...
		    			 copyOfChromosome = (DCNFChromosome) chrom.clone();
		    			 // ...add it to the candidate pool...
		    			 a_candidateChromosomes.add(copyOfChromosome);

		    			 genes = copyOfChromosome.getGenes();
		    		 }

		    		 // Set the new chromosome with fitness -1 so it will be recalculated
		    		 copyOfChromosome.setFitnessValueDirectly(-1);

	    			 DCNFOperandGene toMutate = (DCNFOperandGene)genes[j];
//	    			 mutateGene(toMutate, generator);
	    			 if (toMutate!=null){
//	    				 Formula<String> k = ((Formula<String>) toMutate.getInternalValue());
	    				 toMutate.applyMutation(genes.length, 0);
//	    				 k = ((Formula<String>) toMutate.getInternalValue());
//		    			 k.getClass();
	    			 }

		    	 }
		     }
//		     if (copyOfChromosome!=null){
//			     Formula<String> k = copyOfChromosome.getLTLFormula();
//			     k.getClass();
//		     }
		 }
		
	}
	
	private int getActivePositions(Gene[] genes) {
		int size = 0;
		for (int i=0;i<genes.length;i++) {
			Gene g = genes[i];
			if (g!=null)
				size++;
		}	
		return size;
	}

	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		return 0;
	}

}
