package geneticalgorithm.operator;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.jgap.BaseGeneticOperator;
import org.jgap.Configuration;
import org.jgap.Gene;
import org.jgap.Genotype;
import org.jgap.IChromosome;
import org.jgap.InvalidConfigurationException;
import org.jgap.Population;
import org.jgap.RandomGenerator;

import dCNF.LTL2dCNF;
import geneticalgorithm.chromosome.DCNFChromosome;
import geneticalgorithm.chromosome.DCNFOperandGene;
import gov.nasa.ltl.trans.Formula;
import gov.nasa.ltl.trans.Formula.Content;
import gov.nasa.ltl.trans.ParseErrorException;
import main.BCLearner;

public class DCNFCrossoverOperator extends BaseGeneticOperator implements Comparable {

	public DCNFCrossoverOperator()
		      throws InvalidConfigurationException {
		    super(Genotype.getStaticConfiguration());
		    init();
		  }
		  
		  public DCNFCrossoverOperator(final Configuration a_configuration)
		      throws InvalidConfigurationException {
		    super(a_configuration);
		    init();
		  }
		  

		  /**
		   * Initializes certain parameters.
		   */
		  protected void init() {
		    // Set the default crossoverRate.
		    // ------------------------------
		  }
		  
		@Override
		public void operate(Population a_population, List a_candidateChromosomes) {
			int size = Math.min(getConfiguration().getPopulationSize(), a_population.size());
			Double n = size*(BCLearner.CROSSOVER_RATE/(double)100);
		    int numCrossovers = Math.max(10,n.intValue()); 
		    RandomGenerator generator = getConfiguration().getRandomGenerator();
		    // Choose the two chromosomes randomly
	    	int i = 0;
	    	int index1, index2;
	    	while (i < numCrossovers) {
	    		index1 = generator.nextInt(size);
	    		index2 = generator.nextInt(size);
	    		DCNFChromosome chrom1 = (DCNFChromosome)a_population.getChromosome(index1);
	    		DCNFChromosome chrom2 = (DCNFChromosome)a_population.getChromosome(index2);
	    		if (chrom1.getFitnessValue()>0 && chrom2.getFitnessValue()>0) {
	    			IChromosome firstMate = (IChromosome) chrom1.clone();
	    	    	IChromosome secondMate = (IChromosome) chrom2.clone();
	    	    	// Cross over the chromosomes.
	    	    	// ---------------------------
	    	    	try {
						doCrossover(firstMate, secondMate, a_candidateChromosomes);
					} catch (InvalidConfigurationException | ParseErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}    			
	    			i++;
	    		}
	    	}
		}

		protected void doCrossover(IChromosome firstMate, IChromosome secondMate, List a_candidateChromosomes) throws InvalidConfigurationException, ParseErrorException {
			try {
				Formula<String> f1 = ((DCNFChromosome)firstMate).getLTLFormula().clone();
				Formula<String> f2 = ((DCNFChromosome)secondMate).getLTLFormula().clone();
				DCNFChromosome n =  null;
				RandomGenerator generator = getConfiguration().getRandomGenerator();
				int maxOP = 0;
				if (f1.size()+f2.size()<BCLearner.CHROMOSOMES_SIZE*0.8)
					maxOP = 6;
				else
					maxOP = 4;
				int random = generator.nextInt(maxOP); 
				//0 -> negateF1; 1 -> negateF2; 2 -> unaryOpToF1; 3 -> unaryOpToF2; 4 -> newBinaryOp; 5 -> mergeSubformulas 
				switch (random) {
				case 0: {
					Formula<String> f = f1.clone().negate();
					n = new DCNFChromosome(f.toString(), getConfiguration());
					break;
					}
				case 1: {
					Formula<String> f = f2.clone().negate();
					n = new DCNFChromosome(f.toString(), getConfiguration());
					break;
					}
				case 2: {
					Content c = Content.newUnaryOp(null);
					Formula<String> f = new Formula<String>(c,f1.clone(), null, null);
					n = new DCNFChromosome(f.toString(), getConfiguration());
					break;
					}
				case 3: {
					Content c = Content.newUnaryOp(null);
					Formula<String> f = new Formula<String>(c,f2.clone(), null, null);
					n = new DCNFChromosome(f.toString(), getConfiguration());
					break;
					}
				case 4: {
					Formula<String> f = f1.clone();
					Set<Formula<String>> subf1 = f1.getSubFormulas();
					Set<Formula<String>> subf2 = f2.getSubFormulas();
					int index1 = generator.nextInt(subf1.size());
					int index2 = generator.nextInt(subf2.size());
					f = f.replaceSubFormula((Formula<String>)subf1.toArray()[index1], (Formula<String>)subf2.toArray()[index2]);
//				System.out.println(f.toString());
					n = new DCNFChromosome(f.toString(), getConfiguration());
					break;
					}
				case 5: {
					Content c = Content.newBinaryOp(null);
					Formula<String> f = new Formula<String>(c,f1.clone(), f2.clone(), null);
					n = new DCNFChromosome(f.toString(), getConfiguration());
					break;
					}
				}
				n.setFitnessValueDirectly(-1);
				a_candidateChromosomes.add(n);
			} catch (IllegalArgumentException e) {
				//Do nothing when the chromosome can't created
			}
		}
		
//		protected void doCrossover(IChromosome firstMate, IChromosome secondMate, List a_candidateChromosomes) throws InvalidConfigurationException {
//			Gene[] newgenes = combine(firstMate.getGenes(), secondMate.getGenes());
//			DCNFChromosome n = new DCNFChromosome(getConfiguration(), newgenes);
//			n.setFitnessValueDirectly(-1);
//			a_candidateChromosomes.add(n);
//		}	
		
		public Gene[] combine(Gene[] chrom1, Gene[] chrom2) throws InvalidConfigurationException{
			Gene[] newgenes = new Gene[chrom1.length];
			int i = 0;
			// While the genes are not null, take a gene randomly from chrom1 or chrom2
			while (i < chrom1.length && chrom1[i]!=null && chrom2[i]!=null) {
				// Determine from which chromosome take the i-th gene
				Gene g;
				int random = (new Random()).nextInt(2);
				if (random==0)
					g = chrom1[i];
				else if (random==1)
					g = chrom2[i];
				else
					throw new IllegalStateException();
				newgenes[i] = g;
				i++;
			}
			// Fill the newgenes with the remaining genes
			if (i < chrom1.length) {
				if (chrom1[i]==null) {
					while (i < chrom2.length) {
						newgenes[i] = chrom2[i];
						i++;
					}
				} else {
					while (i < chrom1.length) {
						newgenes[i] = chrom1[i];
						i++;
					}
				}
			}
		
			return newgenes;
		}
		
		int getID(String x){
			int id = 0;
			// TODO VERIFY THIS
			return Integer.valueOf(x.substring(2));
		}
		
		@Override
		public int compareTo(Object o) {
			// TODO Auto-generated method stub
			return 0;
		}
}
