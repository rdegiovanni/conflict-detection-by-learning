package geneticalgorithm.fitnessfunction;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.jgap.FitnessFunction;
import org.jgap.Gene;
import org.jgap.IChromosome;

import dCNF.LTLSolver;
import dCNF.LTLSolver.SolverResult;
import geneticalgorithm.chromosome.DCNFChromosome;
import geneticalgorithm.chromosome.DCNFOperandGene;
import gov.nasa.ltl.trans.Formula;
import gov.nasa.ltl.trans.Formula.Content;

/**
 * A fitness function to determine how close is a dCNF formula to be a Boundary Condition. 
 */
public class DCNFEvaluator extends FitnessFunction {
	Set<Formula<String>> dom = new HashSet<>();
	Set<Formula<String>> goals = new HashSet<>();

	static double LOGICAL_INCONSISTENCY = 1;
	static double MINIMALITY = 1;
	static double NONTRIVIALLITY = 0.5;
	public static boolean check_NONTRIVIALLITY = true;

	public static double MIN_SOLUTION_VALUE = LOGICAL_INCONSISTENCY + MINIMALITY + (check_NONTRIVIALLITY?NONTRIVIALLITY:0);
	public DCNFEvaluator(Set<Formula<String>> dom, Set<Formula<String>> goals) {
		this.dom.addAll(dom);
		this.goals.addAll(goals);
	}
	
	Map<String,Double> formulasAndValues = new HashMap<String,Double>();
	public static int TOTAL_REEVALUATIONS = 0;
	public static int TOTAL_EVALUATIONS = 0;
	
	public double _evaluate(IChromosome arg0) {
		return evaluate(arg0);
	}
	
	@Override
	protected double evaluate(IChromosome arg0) {
		//System.out.println("EVALUATION");
		DCNFChromosome chrom = (DCNFChromosome) arg0;
		
		TOTAL_EVALUATIONS++;
		if (formulasAndValues.containsKey(chrom.toString())) {
			TOTAL_REEVALUATIONS++;
			return formulasAndValues.get(chrom.toString());
		}
		
		
		Set<Formula<String>> s = new HashSet<>();
		Formula<String> pBC = chrom.getLTLFormula();
		
		//check fist if it is equivalent to true or false
		s.add(pBC);
		SolverResult sat = SolverResult.UNSAT;
		try{ sat = checkSAT(s); }
		catch (Exception e) {e.printStackTrace();}
		if (sat!=SolverResult.SAT) {//it is equivalent to false
			formulasAndValues.put(chrom.toString(), 0d);
			return (double)0;
		} else{
			s.clear();
			s.add(Formula.Not(pBC));
			sat = SolverResult.UNSAT;
			try{ sat = checkSAT(s); }
			catch (Exception e) {e.printStackTrace();}
			if (sat!=SolverResult.SAT) {//it is equivalent to true
				formulasAndValues.put(chrom.toString(), 0d);
				return (double)0;
			}
		}
//		System.out.println(pBC.toString());
		double fitness = 0;
		//check logical inconsistency
		s.clear();
		s.addAll(dom);
		s.addAll(goals);
		s.add(pBC);
		sat = SolverResult.UNSAT;
		try{ sat = checkSAT(s); }
		catch (Exception e) {e.printStackTrace();}
		if (sat==SolverResult.UNSAT){
			fitness = LOGICAL_INCONSISTENCY;
			//System.out.println("FITNESS " + fitness);
			System.out.print("i");
		}
		
		//check minimality
//		if (fitness==LOGICAL_INCONSISTENCY) {
			int satisfiedGoals = 0;
			for (Formula<String> g : goals){
				s.clear();
				s.addAll(dom);
				s.addAll(goals);
				s.add(pBC);
				s.remove(g);
				sat = SolverResult.UNSAT;
				try{ sat = checkSAT(s); }
				catch (Exception e) {e.printStackTrace();}
				if (sat==SolverResult.SAT){
					satisfiedGoals++;
					System.out.print("m");
				}
			}
			fitness += (MINIMALITY*satisfiedGoals)/(double)goals.size();
			System.out.println("FITNESS " + fitness);
//		}
		//check non-triviality
		//pBC & Goals
//		s.clear();
//		s.addAll(goals);
//		s.add(pBC);
//		sat = false;
//		try{ sat = checkSAT(s); }
//		catch (Exception e) {e.printStackTrace();}
//		if (sat){
//			fitness += 0.5;
//			System.out.print(">");
//		}
		//Not(pBC) & Not(Goals)
		s.clear();
//		if (fitness==LOGICAL_INCONSISTENCY+MINIMALITY) {
		if (check_NONTRIVIALLITY){
			s.add(Formula.Not(pBC));
		
			boolean first = true;
			Formula<String> allgoals = null; 
			for(Formula<String> g : goals){
				if (first){
					allgoals = g.clone();
					first = false;
				}
				else {
					allgoals = Formula.And(allgoals.clone(), g.clone());
				}
			}
			s.add(Formula.Not(allgoals));
			sat = SolverResult.UNSAT;
			try{ sat = checkSAT(s); }
			catch (Exception e) {e.printStackTrace();}
			if (sat==SolverResult.SAT){
				fitness += NONTRIVIALLITY;
				System.out.print("<");
				//System.out.println("FITNESS " + fitness);
			}
		}
		// Consider the size of the formula (the shorter, the better) when is a solution
		if (fitness>=MIN_SOLUTION_VALUE) {
			int size=0;
			for (Gene g: chrom.getGenes()) {
				if (g!=null)
					size++;
			}
			fitness += (double)(1/(double)size);
			//System.out.println("FITNESS " + fitness);
		}
		System.out.print(";");
		
		//For debug!
		s.clear();
		s.addAll(dom);
		s.addAll(goals);
		s.add(pBC);
		//System.out.println(printToString(s) + " , FITNESS: " + fitness);
		
		formulasAndValues.put(chrom.toString(), fitness);
		return fitness;
	}

	public static String printToString(Set<Formula<String>> formulas) {
		Formula<String> form = null; 
		boolean first = true;
		for(Formula<String> f : formulas){
			if (first){
				form = f;
				first = false;
			}
			else {
				form = Formula.And(form, f);
			}
		}
		return form.toPLTLString();
	}
	
	public static SolverResult checkSAT(Set<Formula<String>> formulas) throws IOException, InterruptedException{
		Formula<String> form = null; 
		boolean first = true;
		for(Formula<String> f : formulas){
			if (first){
				form = f;
				first = false;
			}
			else {
				form = Formula.And(form, f);
			}
		}
		String formulaStr = form.toPLTLString();
		SolverResult sat = LTLSolver.isSAT(formulaStr);
		return sat;
	}
	
}
