package geneticalgorithm.chromosome;

import java.util.Random;

import org.jgap.BaseGene;
import org.jgap.Configuration;
import org.jgap.Gene;
import org.jgap.Genotype;
import org.jgap.InvalidConfigurationException;
import org.jgap.RandomGenerator;
import org.jgap.UnsupportedRepresentationException;

import gov.nasa.ltl.trans.Formula;
import gov.nasa.ltl.trans.Formula.Content;
import gov.nasa.ltl.trans.Parser;

/**
 * A Gene for a DCNFChromosome. A gene represents an operand of the conjunction represented by the
 * DCNFChromosome. 
 */
public class DCNFOperandGene extends BaseGene implements Gene {

	Formula<String> subformula = null;
	
	/**
	 * Default constructor
	 * @throws InvalidConfigurationException
	 */
	public DCNFOperandGene (Formula<String> f,Configuration conf) throws InvalidConfigurationException{
		super(conf);
		subformula = f;
	}
	
	@Override
	public int compareTo(Object o) {
		return subformula.toString().compareTo(((DCNFOperandGene)o).subformula.toString());
	}

	@Override
	public void applyMutation(int arg0, double arg1) {
		if (subformula==null || subformula.getSub2()==null)
			return;
		Formula<String> expr = subformula.getSub2();
		switch (expr.getContent()) {
	    case TRUE: 
		case FALSE: 
		case PROPOSITION: 
		case NOT: //negations should only be applied to propositions
		{
			if (!expr.isLiteral())
				assert false : "negations should only be applied to propositions: " + expr;
			mutateLiteral();
			break;
		}
				
		case NEXT:
	    case GLOBALLY://Renzo: Added to support dcnf notation
	    case FUTURE://Renzo: Added to support dcnf notation
	    	mutateUnaryExpr();
	    	break;
		case AND: 
		case OR:
	    case IFF://Renzo: potential BUG. Added to support dcnf notation		
	   	case UNTIL:
	    case WEAK_UNTIL:
	    case RELEASE:
	    	mutateBinaryExpr();
	    	break;
	    default:
	    	assert false : "mutation failed: " + expr;
	    }
	}
	
	private void mutateLiteral(){
		Formula<String> expr = subformula.getSub2();
		boolean propReplacementApplicable = propositionReplacementApplicable(expr);
		int random = propReplacementApplicable?(new Random()).nextInt(4):(new Random()).nextInt(3); //0 -> FALSE, 1 -> TRUE, 2 -> negate, 3 -> replaceByOtherProp 
		switch (random){
		case 0: {
			Formula<String> f = Formula.False();
			subformula.addRight(f);
			break;
			}
		case 1:{
			Formula<String> f = Formula.True();
			subformula.addRight(f);
			break;
			}
		case 2: {
//			Content n = newUnaryOp(null);
//			Formula<String> f = new Formula<String>(n,expr.clone(), null, null);
			Formula<String> f = expr.clone().negate();
			subformula.addRight(f);
			break;
			}
		case 3: { 
//			int x = Integer.valueOf(subformula.getSub1().getName().substring(2));
			Formula<String> f = getDifferentProp(expr);
			subformula.addRight(f);
			break;
			}
//		case 4: { 
//			int x = Integer.valueOf(subformula.getSub1().getName().substring(2));
//			if (expr == null)
//				expr = getDifferentProp(x,maxID);
//			Content n = newBinaryOp(null);
//			Formula<String> f = new Formula<String>(n,expr.clone(), getDifferentProp(x,maxID), null);
////			Formula<String> f = Formula.And(expr.clone(), getDifferentProp(-1,maxID));
//			subformula.addRight(f);
//			break;
//			}
		}
	}
	
	// Returns true if the proposition p can be replaced by a valid proposition different from p
	private boolean propositionReplacementApplicable(Formula<String> p) {
		if (Parser.propositions.size() <=1 && p!=null){
			return false;
		} 
		for (int i=0; i < Parser.propositions.size(); i++) {
			String n = (String)Parser.propositions.toArray()[i];
			if (!n.startsWith("x_") && (p==null || p.getName()==null || !n.equals(p.getName()))){
				return true;
			}
		}
		return false;
	}
	
	private Formula<String> getDifferentProp(Formula<String> p){
		Formula<String> f = null;
		if (Parser.propositions.size() <=1 && p!=null){
			//if the specification only contain 1 proposition
			return null;
		}
		while (f==null){
			int i = (new Random()).nextInt(Parser.propositions.size());
			String n = (String) Parser.propositions.toArray()[i];
			if (!n.startsWith("x_") && (p==null || p.getName()==null || !n.equals(p.getName()))){
				f = Formula.Proposition(n);
			}
		}
		return f;
	}
//	private Formula<String> getDifferentProp(int p, int maxID){
//		int i = (new Random()).nextInt(maxID+1);
//		while (p==i){
//			i = (new Random()).nextInt(maxID+1);
//		}
//		return Formula.Proposition("x_"+i);
//	}
	
	private void mutateUnaryExpr(){
		Formula<String> expr = subformula.getSub2();
		int random = (new Random()).nextInt(3); 
		//0 -> removeOP; 1 -> changeOp; 2 -> negateFormula
		switch (random) {
//		case 0: {
//			Formula<String> f = Formula.False();
//			subformula.addRight(f);
//			break;
//			}
//		case 1:{
//			Formula<String> f = Formula.True();
//			subformula.addRight(f);
//			break;
//			}
		case 0: { 
			subformula.addRight(expr.getSub1().clone());
			break;
			}
		case 1: { 
			Content n = Content.newUnaryOp(expr.getContent());
			Formula<String> f = new Formula<String>(n,expr.clone().getSub1(), null, null);
			subformula.addRight(f);
			break;
			}
		case 2: { 
			Formula<String> f = expr.clone().negate();
			subformula.addRight(f);
			break;
			}
//		case 5: { 
//			Content n = newBinaryOp(null);
//			int x = Integer.valueOf(subformula.getSub1().getName().substring(2));
//			Formula<String> f = new Formula<String>(n,expr.clone(), getDifferentProp(x,maxID), null);
//			subformula.addRight(f);
//			break;
//			}
		}
	}
	
	
	
	private void mutateBinaryExpr(){
		Formula<String> expr = subformula.getSub2();
		int random = (new Random()).nextInt(3); 
		//0 -> removeOp; 1 -> changeOp; 2 -> negateFormula
		switch (random) {
//		case 0: {
//			Formula<String> f = Formula.False();
//			subformula.addRight(f);
//			break;
//			}
//		case 1:{
//			Formula<String> f = Formula.True();
//			subformula.addRight(f);
//			break;
//			}
		case 0: { 
			int r2 = (new Random()).nextInt(2);
			if (r2==0)
				subformula.addRight(expr.getSub1().clone());
			else
				subformula.addRight(expr.getSub2().clone());
			break;
			}
		case 1: { 
			Content n = Content.newBinaryOp(expr.getContent());
			Formula<String> f = new Formula<String>(n,expr.getSub1().clone(), expr.getSub2().clone(), null);
			subformula.addRight(f);
			break;
			}
		case 2: { 
			Formula<String> f = expr.clone().negate();
			subformula.addRight(f);
			break;
			}
//		case 5: { 
//			Content n = newBinaryOp(null);
//			int x = Integer.valueOf(subformula.getSub1().getName().substring(2));
//			Formula<String> f = new Formula<String>(n,expr.clone(), getDifferentProp(x,maxID), null);
//			subformula.addRight(f);
//			break;
//			}
		}
	}
	
	@Override
	public String getPersistentRepresentation() throws UnsupportedOperationException {
		return subformula.toString();
	}

	@Override
	public void setAllele(Object arg0) {
		subformula = (((Formula<String>) arg0).clone());
	}

	@Override
	public void setToRandomValue(RandomGenerator arg0) {
		subformula = getDifferentProp(null); //TODO: potential bug.
	}

	@Override
	public void setValueFromPersistentRepresentation(String arg0)
			throws UnsupportedOperationException, UnsupportedRepresentationException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Object getInternalValue() {
		return subformula.clone();
	}

	@Override
	public Gene newGeneInternal() {
		try {
			Formula<String> f = subformula.clone();
			return new DCNFOperandGene(f,getConfiguration());
		} catch (InvalidConfigurationException e) {
			throw new IllegalStateException(e.getMessage());
		}
	}
	
	@Override
	public String toString(){
		if (subformula==null)
			return "gene null";
		else
			return subformula.toString();
	}

}
