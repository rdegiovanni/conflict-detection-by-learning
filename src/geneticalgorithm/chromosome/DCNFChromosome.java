package geneticalgorithm.chromosome;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import org.jgap.BaseChromosome;
import org.jgap.Configuration;
import org.jgap.FitnessEvaluator;
import org.jgap.FitnessFunction;
import org.jgap.Gene;
import org.jgap.Genotype;
import org.jgap.IChromosome;
import org.jgap.IChromosomePool;
import org.jgap.ICloneHandler;
import org.jgap.IGeneConstraintChecker;
import org.jgap.IJGAPFactory;
import org.jgap.InvalidConfigurationException;

import dCNF.LTL2dCNF;
import gov.nasa.ltl.trans.Formula;
import gov.nasa.ltl.trans.Formula.Content;
import gov.nasa.ltl.trans.ParseErrorException;
import main.BCLearner;

/**
 * A Chromosome representing a dCNF (definitional conjunctive normal form) formula. A dCNF formula is a conjunction 
 * representing a particular LTL formula. Each gene of a chromosome is an operand of the conjunction.
 */
public class DCNFChromosome extends BaseChromosome {

	// Last calculated fitness value of this Chromosome
	protected double m_fitnessValue = FitnessFunction.NO_FITNESS_VALUE;

	private boolean m_isSelectedForNextGeneration;

	private Object m_geneAlleleChecker;
	
	public DCNFChromosome(String f, Configuration a_configuration) throws InvalidConfigurationException, ParseErrorException {
		super(a_configuration);
		LTL2dCNF formula = new LTL2dCNF(f);
		formula.dCNF(); //generate the dCNF representation of f
		Gene[] a_initialGenes = new Gene[BCLearner.CHROMOSOMES_SIZE]; //TODO: check chromosome size
		
		List<Formula<String>> dcnf = formula.getDCNF();
		if (dcnf.size()>BCLearner.CHROMOSOMES_SIZE)
			throw new IllegalArgumentException("The dCNF representation of the given formula can't be described by a chromosome of length "+BCLearner.CHROMOSOMES_SIZE);
		
		for(int i = 0; i< dcnf.size(); i++){
			Formula<String> c = dcnf.get(i);
			Gene g = new DCNFOperandGene(c,a_configuration);
			a_initialGenes[i] = g;
		}
		// Fill the remaining genes with True
//		while (i < a_initialGenes.length) {
//			Gene g = new DCNFOperandGene(Formula.True(),a_configuration);
//			a_initialGenes[i] = g;
//			i++;
//		}
		setGenes(a_initialGenes);
	}
	
	public DCNFChromosome(Configuration a_configuration, Gene[] a_initialGenes) throws InvalidConfigurationException {
		super(a_configuration);
	    setGenes(a_initialGenes);
	}
	 
	public DCNFChromosome(Configuration a_configuration) throws InvalidConfigurationException {
		super(a_configuration);
	}
	
	//Get the formula characterised by the chromosome
	public Formula<String> getDCNFFormula(){
		Formula<String> x = Formula.Proposition("x_0");
		boolean first = true;
		Formula<String> form = null; 
		
		for(Gene g : this.getGenes()){
			if (g == null)
				continue;
			Formula<String> f = (Formula<String>) ((DCNFOperandGene) g).getInternalValue();
			if (first){
				form = f;
				first = false;
			}
			else {
				form = Formula.And(form, f);
			}
		}
		Formula<String> pBC = Formula.And(x,Formula.Always(form));
		return pBC;
	}
	
	//Get the formula characterised by the chromosome
		public Formula<String> getLTLFormula(){
			boolean first = true;
			Formula<String> form = null; 
			Gene[] genes = this.getGenes();
			List<Integer> xs = new LinkedList<>();
			xs.add(0);
			while(!xs.isEmpty()){
				int x = xs.remove(0);
				Formula<String> f = (Formula<String>) ((DCNFOperandGene) genes[x]).getInternalValue();
				if (first){
					form = f.getSub2().clone();
					first = false;
				}
				else {
					form = form.replaceSubFormula(f.getSub1(), f.getSub2());
				}
				for (Integer i : getNextFormulas(form))
					xs.add(i);
				
			}
//				Gene g = genes[i]; 
//				if (g == null)
//					continue;
//				Formula<String> f = (Formula<String>) ((DCNFOperandGene) g).getInternalValue();
				
			return form;
		}
		
	private List<Integer> getNextFormulas(Formula<String> f){
		List<Integer> xs = new LinkedList<>();
		for (Formula<String> s : f.getSubFormulas()){
			if (s.getContent()==Content.PROPOSITION && s.getName().startsWith("x_")){
				int x = Integer.valueOf(s.getName().substring(2));
				xs.add(x);
			}
		}
		return xs;
	}
	
	@Override
	public double getFitnessValue() {
		if (m_fitnessValue >= 0.000d) {
			return m_fitnessValue;
		}
		else {
			return calcFitnessValue();
	    }
	}
	
	 /**
	 * @return fitness value of this chromosome determined via the registered
	 * fitness function
	 */
	protected double calcFitnessValue() {
	    if (getConfiguration() != null) {
	      FitnessFunction normalFitnessFunction = getConfiguration().getFitnessFunction();
	      if (normalFitnessFunction != null) {
	        // Grab the "normal" fitness function and ask it to calculate our
	        // fitness value.
	        // --------------------------------------------------------------
	        m_fitnessValue = normalFitnessFunction.getFitnessValue(this);
	      }
	    }
	    return m_fitnessValue;
	}

	@Override
	public double getFitnessValueDirectly() {
		return m_fitnessValue;
	}

	@Override
	public void setIsSelectedForNextGeneration(boolean a_isSelected) {
	    m_isSelectedForNextGeneration = a_isSelected;
	}

	@Override
	public boolean isSelectedForNextGeneration() {
		return m_isSelectedForNextGeneration;
	}

	@Override
	public void setConstraintChecker(IGeneConstraintChecker a_constraintChecker) throws InvalidConfigurationException {
		verify(a_constraintChecker);
		m_geneAlleleChecker = a_constraintChecker;
	}
	
	/**
	 * Verifies the state of the chromosome. Especially takes care of the
	 * given constraint checker.
	 */
	protected void verify(IGeneConstraintChecker a_constraintChecker) throws InvalidConfigurationException {
		if (a_constraintChecker != null && getGenes() != null) {
			int len = getGenes().length;
			for (int i = 0; i < len; i++) {
	        Gene gene = getGene(i);
	        if (!a_constraintChecker.verify(gene, null, this, i)) {
	          throw new InvalidConfigurationException(
	              "The gene type "
	              + gene.getClass().getName()
	              + " is not allowed to be used in the chromosome due to the"
	              + " constraint checker used.");
	        }
	      }
	    }
	}
	
	public IGeneConstraintChecker getConstraintChecker() {
		return null;
	}

	@Override
	public void setFitnessValue(double a_newFitnessValue) {
		if (a_newFitnessValue >= 0 && Math.abs(m_fitnessValue - a_newFitnessValue) > 0.0000001) {
			m_fitnessValue = a_newFitnessValue;
		}
	}

	@Override
	public void setFitnessValueDirectly(double a_newFitnessValue) {
		m_fitnessValue = a_newFitnessValue;
	}

	@Override
	public int compareTo(Object other) {
		// First, if the other Chromosome is null, then this chromosome is
	    // automatically the "greater" Chromosome.
	    // ---------------------------------------------------------------
	    if (other == null) {
	      return 1;
	    }
	    int size = size();
	    IChromosome otherChromosome = (IChromosome) other;
	    Gene[] otherGenes = otherChromosome.getGenes();
	    // If the other Chromosome doesn't have the same number of genes,
	    // then whichever has more is the "greater" Chromosome.
	    // --------------------------------------------------------------
	    if (otherChromosome.size() != size) {
	      return size() - otherChromosome.size();
	    }
	    // Next, compare the gene values (alleles) for differences. If
	    // one of the genes is not equal, then we return the result of its
	    // comparison.
	    // ---------------------------------------------------------------
	    for (int i = 0; i < size; i++) {
	      int comparison = getGene(i).compareTo(otherGenes[i]);
	      if (comparison != 0) {
	        return comparison;
	      }
	    }
	    // Compare current fitness value.
	    // ------------------------------
	    if (m_fitnessValue != otherChromosome.getFitnessValueDirectly()) {
	      FitnessEvaluator eval = getConfiguration().getFitnessEvaluator();
	      if (eval != null) {
	        if (eval.isFitter(m_fitnessValue,
	                          otherChromosome.getFitnessValueDirectly())) {
	          return 1;
	        }
	        else {
	          return -1;
	        }
	      }
	      else {
	        // undetermined order, but unequal!
	        // --------------------------------
	        return -1;
	      }
	    }
	    // Everything is equal. Return zero.
	    // ---------------------------------
	    return 0;
	}

	@Override
	public boolean isHandlerFor(Object arg0, Class arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Object perform(Object arg0, Class arg1, Object arg2) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object clone() {
		// Before doing anything, make sure that a Configuration object
	    // has been set on this Chromosome. If not, then throw an
	    // IllegalStateException.
	    // ------------------------------------------------------------
		if (getConfiguration() == null) {
	      throw new IllegalStateException(
	          "The active Configuration object must be set on this " +
	          "Chromosome prior to invocation of the clone() method.");
	    }
		Gene[] newGenes = new Gene[BCLearner.CHROMOSOMES_SIZE]; //TODO: check chromosome size
		Gene[] genes = getGenes();
		for (int i=0; i<genes.length; i++){
			if(genes[i]!=null)
				newGenes[i] = ((DCNFOperandGene)genes[i]).newGeneInternal();
		}
	    DCNFChromosome copy=null;
		try {
			copy = new DCNFChromosome(getConfiguration(), newGenes);
			copy.setFitnessValue(m_fitnessValue);
		} catch (InvalidConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    return copy;
//	    // Now, first see if we can pull a Chromosome from the pool and just
//	    // set its gene values (alleles) appropriately.
//	    // ------------------------------------------------------------
//	    IChromosomePool pool = getConfiguration().getChromosomePool();
//	    if (pool != null) {
//	      copy = (DCNFChromosome)pool.acquireChromosome();
//	      if (copy != null) {
//	        Gene[] genes = copy.getGenes();
//	        for (int i = 0; i < size(); i++) {
//	          genes[i].setAllele(getGene(i).getAllele());
//	        }
//	      }
//	    }
//	    try {
//	      if (copy == null) {
//	        // We couldn't fetch a Chromosome from the pool, so we need to create
//	        // a new one. First we make a copy of each of the Genes. We explicity
//	        // use the Gene at each respective gene location (locus) to create the
//	        // new Gene that is to occupy that same locus in the new Chromosome.
//	        // -------------------------------------------------------------------
//	        int size = size();
//	        if (size > 0) {
//	          Gene[] copyOfGenes = new Gene[size];
//	          for (int i = 0; i < copyOfGenes.length; i++) {
//	        	  	Gene g = getGene(i);
//	        	  	if (g==null)
//	        	  		continue;
//		            copyOfGenes[i] = g.newGene();
//		            Object allele = g.getAllele();
//		            if (allele != null) {
//		              IJGAPFactory factory = getConfiguration().getJGAPFactory();
//		              if (factory != null) {
//		                ICloneHandler cloner = factory.
//		                    getCloneHandlerFor(allele, allele.getClass());
//		                if (cloner != null) {
//		                  try {
//		                    allele = cloner.perform(allele, null, this);
//		                  } catch (Exception ex) {
//		                    throw new RuntimeException(ex);
//		                  }
//		                }
//		              }
//		            }
//		            copyOfGenes[i].setAllele(allele);
//	          }
//	          // Now construct a new Chromosome with the copies of the genes and
//	          // return it. Also clone the IApplicationData object later on.
//	          // ---------------------------------------------------------------
//	          if (getClass() == DCNFChromosome.class) {
//	            copy = new DCNFChromosome(getConfiguration(), copyOfGenes);
//	          }
//	          else {
//	            copy = (DCNFChromosome) getConfiguration().getSampleChromosome().clone();
//	            copy.setGenes(copyOfGenes);
//	          }
//	        }
//	        else {
//	          if (getClass() == DCNFChromosome.class) {
//	            copy = new DCNFChromosome(getConfiguration());
//	          }
//	          else {
//	            copy = (DCNFChromosome) getConfiguration().getSampleChromosome().clone();
//	          }
//	        }
//	      }
//	      copy.setFitnessValue(m_fitnessValue);
//	      // Clone constraint checker.
//	      // -------------------------
//	      copy.setConstraintChecker(getConstraintChecker());
//	    } catch (InvalidConfigurationException iex) {
//	      throw new IllegalStateException(iex.getMessage());
//	    }
//	    // Also clone the IApplicationData object.
//	    // ---------------------------------------
	    
//	    return copy;
	}
	
	@Override
	public void cleanup() {
	    if (getConfiguration() == null) {
	      throw new IllegalStateException(
	          "The active Configuration object must be set on this " +
	          "Chromosome prior to invocation of the cleanup() method.");
	    }
	    // First, reset our internal state.
	    // --------------------------------
	    m_fitnessValue = getConfiguration().getFitnessFunction().NO_FITNESS_VALUE;
//	    m_isSelectedForNextGeneration = false;
	    // Next we want to try to release this Chromosome to a ChromosomePool
	    // if one has been setup so that we can save a little time and memory
	    // next time a Chromosome is needed.
	    // ------------------------------------------------------------------
	    // Now fetch the active ChromosomePool from the Configuration object
	    // and, if the pool exists, release this Chromosome to it.
	    // -----------------------------------------------------------------
	    IChromosomePool pool = getConfiguration().getChromosomePool();
	    if (pool != null) {
	      // Note that the pool will take care of any gene cleanup for us,
	      // so we don't need to worry about it here.
	      // -------------------------------------------------------------
	      pool.releaseChromosome(this);
	    }
	    else {
	      // No pool is available, so we need to finish cleaning up, which
	      // basically entails requesting each of our genes to clean
	      // themselves up as well.
	      // -------------------------------------------------------------
	      for (int i = 0; i < size(); i++) {
	        getGene(i).cleanup();
	      }
	    }
	}
	
	@Override
	public Object getApplicationData() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void setApplicationData(Object arg0) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public String toString(){
		String s = "[";
		for(Gene g : getGenes()){
			if (g!=null)
				s += g.toString()+",";
		}
		return s;
	}
}
