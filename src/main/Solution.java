package main;

import gov.nasa.ltl.trans.Formula;

public class Solution {

	public Formula<String> BC = null;
	double fitness = 0d;
	int time = 0;
	int iteration = 0;
	
	public Solution(Formula<String> f, double fit, int t, int it){
		BC = f;
		fitness = fit;
		time = t;
		iteration = it;
	}
	
	public String getFormulaString(){
		return BC.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		return BC!=null && BC.equals(((Solution)obj).BC);
	}
	
}
