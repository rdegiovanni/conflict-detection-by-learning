package main;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.jgap.Configuration;
import org.jgap.Gene;
import org.jgap.Genotype;
import org.jgap.InvalidConfigurationException;
import org.jgap.impl.DefaultConfiguration;
import org.jgap.impl.TournamentSelector;

import dCNF.LTLSolver;
import dCNF.LTLSolver.SolverResult;
import geneticalgorithm.chromosome.DCNFChromosome;
import geneticalgorithm.chromosome.DCNFOperandGene;
import geneticalgorithm.fitnessfunction.DCNFEvaluator;
import geneticalgorithm.operator.DCNFCrossoverOperator;
import geneticalgorithm.operator.DCNFMutator;
import gov.nasa.ltl.trans.Formula;
import gov.nasa.ltl.trans.ParseErrorException;
import gov.nasa.ltl.trans.Parser;

/**
 * Main class for the Boundary Conditions learner
 */
public class BCLearner {

	public Configuration conf = new DefaultConfiguration();
	Set<Formula<String>> dom = new HashSet<>();
	Set<Formula<String>> goals = new HashSet<>();
	
	public static int CHROMOSOMES_SIZE = 200;
	static int GENERATIONS = 20;
	public static int LIMIT_SOLUTIONS = 1000000;
	static int POPULATION_SIZE = 100;
	public static int CROSSOVER_RATE = 10; // Population percentage that will be selected for crossover
	
	/**
	 * Default constructor
	 * @throws ParseErrorException 
	 */
	public BCLearner(Set<String> dom, Set<String> goals) throws InvalidConfigurationException, ParseErrorException {
		DefaultConfiguration.reset();
		for(String d : dom)
			this.dom.add(Parser.parse(d));
		for(String g : goals)
			this.goals.add(Parser.parse(g));
		setUpGeneticAlgorithm();
	}
		
	/**
	 * Set up the genetic algorithm
	 * @throws InvalidConfigurationException
	 */
	private void setUpGeneticAlgorithm() throws InvalidConfigurationException {
		Gene[] sampleGenes;
		sampleGenes =  new Gene[CHROMOSOMES_SIZE];
		// Use IntegerGene
		for (int i = 0; i<CHROMOSOMES_SIZE; i++) {
			Formula<String> fs = Formula.True();
			sampleGenes[i] = new DCNFOperandGene(fs,conf);
		}

		DCNFChromosome sampleChromosome = new DCNFChromosome(conf, sampleGenes);
		conf.setSampleChromosome( sampleChromosome );
		conf.setKeepPopulationSizeConstant(false);
		conf.setPreservFittestIndividual(true);
	}
	
	/**
	 * Get the list of initial chromosomes
	 * @throws ParseErrorException 
	 * @throws InvalidConfigurationException 
	 */
	private List<DCNFChromosome> getInitialChromosomes() throws InvalidConfigurationException, ParseErrorException {
		List<DCNFChromosome> ichromosomes = new LinkedList<>();
		
//		for (Formula<String> g : goals){
//			DCNFChromosome c = new DCNFChromosome("!"+g.toString(), conf);
//			ichromosomes.add(c);
//		}
		
		Set<String> s = new HashSet<>();
		for (Formula<String> g : goals){
			for(Formula<String> f : g.getSubFormulas())
				s.add(f.toString());
		}
		for (Formula<String> g : dom){
			for(Formula<String> f : g.getSubFormulas())
				s.add(f.toString());
		}
		
		for (String f : s){
//			System.out.println(f);
			DCNFChromosome c = new DCNFChromosome(f, conf);
			ichromosomes.add(c);
			c = new DCNFChromosome("!"+f, conf);
			ichromosomes.add(c);
		}
//		for (String p : Parser.propositions){
//			if(!p.startsWith("x_")){
//				DCNFChromosome c = new DCNFChromosome(p, conf);
//				ichromosomes.add(c);
//				c = new DCNFChromosome("!"+p, conf);
//				ichromosomes.add(c);
//			}
//		}

		return ichromosomes;
	}
	
	/**
	 * Returns true iff the given chromosome is a solution
	 */
	private boolean isSolution(DCNFChromosome chrom) {
		//>=2 if non-triviality is checked
		if (chrom.getFitnessValueDirectly()>=DCNFEvaluator.MIN_SOLUTION_VALUE)
			return true;
		return false; 
	}
	
	List<Solution> solutions = new LinkedList<>();
	List<Solution> nonEqSolutions = null;
	List<Solution> weakest = null;
	/**
	 * Run a Genetic Algorithm to find Boundary Conditions
	 * @throws ParseErrorException 
	 */
	public void learnBC() throws InvalidConfigurationException, ParseErrorException {
		
		conf.getGeneticOperators().remove(1); // Delete mutation
		conf.getGeneticOperators().remove(0); // Delete crossover
		conf.getGeneticOperators().add(0, new DCNFMutator(conf));
		conf.getGeneticOperators().add(new DCNFCrossoverOperator(conf));
		
		//conf.removeNaturalSelectors(false);
		//conf.addNaturalSelector(new WeightedRouletteSelector(conf), false);
		//conf.addNaturalSelector(new TournamentSelector(conf, 10, .2d), false);
		//conf.getJGAPFactory().registerCloneHandler(new ExprGeneValueCloneHandler());
		conf.setFitnessFunction(new DCNFEvaluator(dom,goals)); 
		
		List<DCNFChromosome> initialChromosomes = getInitialChromosomes(); 
		conf.setPopulationSize(POPULATION_SIZE); // TODO Discuss an adequate number for the population size
		System.out.println("Initial population size: "+initialChromosomes.size());
		System.out.println("Total population size: "+conf.getPopulationSize());

		Genotype population = Genotype.randomInitialGenotype(conf);
		population.getPopulation().setChromosomes(initialChromosomes);
		
		// Best Chromosome
		DCNFChromosome bestChromosome=null;
		DCNFChromosome foundChromosome=null;
		double current=0;
		int i=0;
		double bestSolution = 0;
		System.out.println();
		// Evolve and end if reach max iterations 
		long itime = System.currentTimeMillis();
		int numOfIterationCalls = 0;
		try{
		//while (i < GENERATIONS) {
		while (i < GENERATIONS && solutions.size() < LIMIT_SOLUTIONS) {
			//How to limit the solution, we need to check if the solution is only used by this.
			//weakest - May we wait?
			long iterationTime = System.currentTimeMillis();
			System.out.println("GENERATION "+(i+1)+"/"+GENERATIONS);
			// Evolve the population
			population.evolve();
			// Get the fittest chromosome
			current=population.getFittestChromosome().getFitnessValueDirectly();
			foundChromosome = (DCNFChromosome)population.getFittestChromosome();	
			i++;
			System.out.println();
			if (isSolution(foundChromosome)) {
				System.out.println("Fittest value: "+current+" for chromosome "+foundChromosome.getLTLFormula());
				getCurrentSolutions(population, ((int)(System.currentTimeMillis()-itime)/1000), i);
				String stringSolutions = "Solutions (" + solutions.size() + "): ";
				for (Solution f : solutions) {
					stringSolutions += f.getFormulaString() + " | ";
				}	
				System.out.println(stringSolutions);
			}
			else {
				System.out.println("Best fitness value: "+foundChromosome.getLTLFormula() + " " + foundChromosome.getFitnessValueDirectly());
			}
			int itTotalTime = (int)(System.currentTimeMillis()-iterationTime)/1000;
			System.out.println("Generation Time :"+itTotalTime);
			System.out.println("Accumulated Learning Time :"+((int)(System.currentTimeMillis()-itime)/1000));
			System.out.println("CALLs: " + (LTLSolver.numOfCalls-numOfIterationCalls) +"    TIMEOUTs: "+LTLSolver.numOfTimeout + "    ERRORs: "+LTLSolver.numOfError);
			System.out.println("REEVALUATIONS: "+DCNFEvaluator.TOTAL_REEVALUATIONS + " OF " + DCNFEvaluator.TOTAL_EVALUATIONS);
			numOfIterationCalls = LTLSolver.numOfCalls;
			System.out.println();
		};
		}
		finally{
		System.out.println();
		int totalTime = (int)(System.currentTimeMillis()-itime)/1000;
		System.out.println("TOTAL Learning Time :"+totalTime);
		System.out.println();
		System.out.println("Solutions: " + solutions.size() );
		for (Solution f : solutions){
			System.out.println(f.getFormulaString());
		}
		System.out.println();
		nonEqSolutions = getNonEquivalentSolutions();
		System.out.println();
		System.out.println("Non Equivalent Solutions: " + nonEqSolutions.size() );
		for (Solution f : nonEqSolutions){
			System.out.println(f.getFormulaString());
		}
		weakest = getWeakestSolutions(nonEqSolutions);
		System.out.println();
		System.out.println("Weakest Solutions: " + weakest.size() );
		for (Solution f : weakest){
			System.out.println(f.getFormulaString());
		}
		totalTime = (int)(System.currentTimeMillis()-itime)/1000;
		
		System.out.println("TOTAL Time :"+totalTime);
		System.out.println("CALLs: " + LTLSolver.numOfCalls +"    TIMEOUTs: "+LTLSolver.numOfTimeout + "    ERRORs: "+LTLSolver.numOfError);
		System.out.println("CONFIGURATION");
		System.out.println("#Population: "+conf.getPopulationSize() + "    #CrossoverRate: " + BCLearner.CROSSOVER_RATE + "    #Iterations: "+GENERATIONS + "    #ChromSize: "+CHROMOSOMES_SIZE);
		System.out.println("REEVALUATIONS: "+DCNFEvaluator.TOTAL_REEVALUATIONS + " OF " + DCNFEvaluator.TOTAL_EVALUATIONS);
		if(!solutions.isEmpty())
			computeStatistics(totalTime);
		}
	}
	
	private void getCurrentSolutions(Genotype population, int time, int it) {
		List<DCNFChromosome> chromosomes = population.getPopulation().getChromosomes();
		for (DCNFChromosome c : chromosomes) {
			if (isSolution(c) ) {
				Solution s = new Solution(c.getLTLFormula(), c.getFitnessValueDirectly(), time, it);
				if (!solutions.contains(s)) {
					solutions.add(s);
				}
			}
		}
	}
	
	private Solution equivalentToSomeSolution(Formula<String> f2,List<Solution> sol) {
		for (Solution f1 : sol) {
			Set<Formula<String>> s = new HashSet<>();
			
			//!f1 && f2
			s.add(Formula.Not(f1.BC));
			s.add(f2);
			SolverResult sat = SolverResult.UNSAT;
			try{ sat = DCNFEvaluator.checkSAT(s); }
			catch (Exception e) {e.printStackTrace();}
			if (sat==SolverResult.SAT)//are not equivalent
				continue;
			else{
				s.clear();
				// f1 && !f2
				s.add(Formula.Not(f2));
				s.add(f1.BC);
				sat = SolverResult.UNSAT;
				try{ sat = DCNFEvaluator.checkSAT(s); }
				catch (Exception e) {e.printStackTrace();}
				if (sat==SolverResult.SAT)//are not equivalent
					continue;
				else
					return f1;
			}
		}
		return null;
	}
	
	private List<Solution> getNonEquivalentSolutions() {
		List<Solution> nonEqSolutions = new LinkedList();
		for (Solution solution : solutions) {
			Solution equiv = equivalentToSomeSolution(solution.BC,nonEqSolutions);
			if (equiv==null) { 
				nonEqSolutions.add(solution);
			}
			else{
				if (solution.BC.size()<equiv.BC.size()){
					nonEqSolutions.remove(equiv);
					nonEqSolutions.add(solution);
				}
			}
		}
		return nonEqSolutions;
	}
	
	private List<Solution> getWeakestSolutions(List<Solution> equivalentSolutions) {
		List<Solution> weakest = new LinkedList();
		for (int i=0;i<equivalentSolutions.size();i++) {
			Solution f1 = equivalentSolutions.get(i);
			boolean f1IsImplied = false;
			for (int j=0; j <equivalentSolutions.size(); j++ ) {
				if (i==j)
					continue;
				Solution f2 = equivalentSolutions.get(j);
				Set<Formula<String>> s = new HashSet<>();
				s.add(f1.BC);
				s.add(Formula.Not(f2.BC));
				SolverResult sat = SolverResult.UNSAT;
				try{ sat = DCNFEvaluator.checkSAT(s); }
				catch (Exception e) {e.printStackTrace();}
				if (sat==SolverResult.UNSAT) {
					f1IsImplied = true;
					break;
				} 
			}
			if (!f1IsImplied)
				weakest.add(f1);
		}
		return weakest;
	}
	
	
	void computeStatistics(int totalTime){
		Solution best = null;
		for (Solution s : solutions){
			if (best==null)
				best = s;
			else if (best.fitness<s.fitness)
				best = s;
		}
		Solution first = solutions.get(0);
		System.out.println("Min. " + first.iteration + " - Accumulated Learning Time : " + first.time + " - Best. "+best.iteration+ " Accumulated Learning Time: "+best.time+" Total time: "+totalTime+ " Sols: "+solutions.size()+" NonEq. " + nonEqSolutions.size());
	}
	
}
