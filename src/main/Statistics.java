package main;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import gov.nasa.ltl.trans.Formula;
import gov.nasa.ltl.trans.ParseErrorException;
import gov.nasa.ltl.trans.Parser;

public class Statistics {
	
	public static String[] loadFile(String filename, int iterations) throws IOException{
		BufferedReader br = new BufferedReader(new FileReader(filename));
		String[] arr = new String[iterations];
		try {
		    int i = -1;
		    String line = br.readLine();
		    while (line != null) {
		    	if (line.startsWith("GENERATION")){
		    		i++;
		    		arr[i] = line;
		    	}
		    	else{
		    		if (i>=0)
		    			arr[i] += "\n"+line;
		    	}
		    	
		    	line = br.readLine();
		    }
		}
		finally {
	    br.close();
		}
		return arr;
	}
	
	public static List<Solution> getSolutions(String [] arr) throws ParseErrorException{
		List<Solution> solutions = new LinkedList<>();
	    
	    for (int i=0 ; i<arr.length;i++){
	    	if (arr[i].contains("Fittest")){
	    		String [] gen = arr[i].split("Accumulated Learning Time :");
	    		int time = Integer.valueOf(gen[1].split("\n")[0]);
	    		gen = arr[i].split("Fittest value: ");
	    		double fitness = Double.valueOf(gen[1].split(" ")[0]);
	    		gen = arr[i].split("for chromosome ");
	    		Formula<String> f = Parser.parse(gen[1].split("\n")[0]);
	    		Solution s = new Solution(f, fitness, time, i+1);
	    		solutions.add(s);
	    	}
	    }
	    return solutions;
	}
	
	public static int getTotalTime (String line){
		if (line.contains("TOTAL Learing Time :"))
			return Integer.valueOf(line.split("TOTAL Learing Time :")[1].split("\n")[0]);
		return 0;
	}
	
	public static int getNumOfSol (String line){
		if (line.contains("Solutions: "))
			return Integer.valueOf(line.split("Solutions: ")[1].split("\n")[0]);
		return 0;
	}
	
	public static int getEqSol (String line){
		if (line.contains("Non Equivalent Solutions: "))
			return Integer.valueOf(line.split("Non Equivalent Solutions: ")[1].split("\n")[0]);
		return 0;
	}

}
