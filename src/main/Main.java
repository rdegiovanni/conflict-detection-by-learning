package main;

import java.util.HashSet;
import java.util.Set;

import org.jgap.InvalidConfigurationException;

import gov.nasa.ltl.trans.ParseErrorException;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import geneticalgorithm.fitnessfunction.*;
public class Main {
	
	public static void main(String[] args) throws InvalidConfigurationException, ParseErrorException, IOException, InterruptedException{
		Set<String> dom = new HashSet();
		Set<String> goals = new HashSet();
		
		int popSize = 0;
		int crossoverRate = 0;
		String outfileWeakest = "";
		for (int i = 0; i< args.length; i++ ){
			if(args[i].startsWith("-d=")){
				dom.add(args[i].replace("-d=", ""));
			}
			else if(args[i].startsWith("-g=")){
				goals.add(args[i].replace("-g=", ""));
			}
			else if (args[i].startsWith("-fileD=")){ 
				String dom_references = args[i].replace("-fileD=","");
				Path pathToFile = Paths.get(dom_references);
				List<String> allLines = Files.readAllLines(pathToFile.toAbsolutePath());
				for (String line : allLines) {
					if (line.isEmpty() || line == "" || line.startsWith("--"))
						continue;
					dom.add("[]("+line+")");
				}
			}
			else if (args[i].startsWith("-fileG=")){ 
				String goals_references = args[i].replace("-fileG=","");
				System.out.println(goals_references);
				Path pathToFile = Paths.get(goals_references);
				List<String> allLines = Files.readAllLines(pathToFile.toAbsolutePath());
				for (String line : allLines) {
					if (line.isEmpty() || line == "" || line.startsWith("--"))
						continue;
					goals.add("[]("+line+")");
				}
			}
			else if (args[i].startsWith("-noCheckTriv")) {
				DCNFEvaluator.check_NONTRIVIALLITY = false;
			}
			else if (args[i].startsWith("-sol=")) {
				DCNFEvaluator.MIN_SOLUTION_VALUE = Double.valueOf(args[i].replace("-sol=", ""));
			}
			else if(args[i].startsWith("-ps=")) {
				popSize = Integer.parseInt(args[i].replace("-ps=", ""));
			}
			else if(args[i].startsWith("-cr=")) {
				crossoverRate = Integer.parseInt(args[i].replace("-cr=", ""));
			}
			else if (args[i].startsWith("-id=")) {
				dCNF.LTLSolver.SATID = args[i].replace("-id=", "");
				dCNF.LTLSolver.init();
			} else if (args[i].startsWith("-outfileWeakest=")) {
				outfileWeakest = args[i].replace("-outfileWeakest=", "");
			} else {
				correctUssage();
				return;
			}
		}
		
		if (goals.isEmpty()){
			correctUssage();
			return;
		}
		System.out.println("DOM: "+dom);
		System.out.println("GOALS: "+goals);
		
		BCLearner learner = new BCLearner(dom, goals);
		if (popSize>0)
			BCLearner.POPULATION_SIZE = popSize;
		if (crossoverRate>0)
			BCLearner.CROSSOVER_RATE = crossoverRate;
		learner.learnBC();
		dCNF.LTLSolver.clear();
		
		if (outfileWeakest != "") {
			/**
			System.out.println("solutions");
			try(FileWriter fw = new FileWriter(outfileWeakest, true)) {
				fw.write("solutions\n");
			}
			for (Solution s : learner.solutions) {
				try(FileWriter fw = new FileWriter(outfileWeakest, true)) {
					fw.write(s.getFormulaString() + "\n");
				}
			}
			
			System.out.println("nonEqSolutions");
			try(FileWriter fw = new FileWriter(outfileWeakest, true)) {
				fw.write("nonEqSolutions\n");
			}
			for (Solution s : learner.nonEqSolutions) {
				try(FileWriter fw = new FileWriter(outfileWeakest, true)) {
					fw.write(s.getFormulaString() + "\n");
				}
			}
			
			System.out.println("weakest");
			try(FileWriter fw = new FileWriter(outfileWeakest, true)) {
				fw.write("weakest\n");
			}
			**/
			for (Solution s : learner.weakest) {
				try(FileWriter fw = new FileWriter(outfileWeakest, true)) {
					fw.write(s.getFormulaString() + "\n");
				}
			}
		}

	}
	
	private static void correctUssage(){
		System.out.println("Use -d=domain -g=Goal");
	}

}
