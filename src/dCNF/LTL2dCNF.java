package dCNF;


import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import gov.nasa.ltl.trans.Formula;
import gov.nasa.ltl.trans.ParseErrorException;
import gov.nasa.ltl.trans.Parser;

public class LTL2dCNF {
	
	String formula = "";
	Formula<String> ltl = null;
	List<Formula<String>> dcnf = dcnf = new LinkedList<>();

	public LTL2dCNF(){
		init();
	}
	public LTL2dCNF(String f) throws ParseErrorException{
		init();
		formula = f.toString();
		ltl = Parser.parse(formula);
	}
	
	public Formula<String> toPNF(){
		return toPNF(ltl, false);
	}
	
	private void init(){
		dcnf.clear();
		id = 0;
		ids.clear();
		formula = "";
		ltl = null;
	}
	//It translate any LTL formula to PNF
	//Maybe not necessary, Formula.Not method consider these cases.
	public Formula<String> toPNF(Formula<String> f, boolean negate){
		if (f.isLiteral()){
			if(negate){
				if (f.getContent()==Formula.Content.NOT)
					return f.getSub1();
				else
					return f.negate();
			}
			else
				return f;
		}
		else{
			if (f.getContent() == Formula.Content.NOT) {
				Formula<String> fnot = f.getSub1();
				Formula<String> f1 = toPNF(fnot.getSub1(),!negate);
				Formula<String> f2 = null;
				if (f.getSub2()!=null){
					f2 = toPNF(fnot.getSub2(),!negate);
				}
				switch (fnot.getContent()) {
				case AND: {
					new Formula<String>(Formula.Content.OR, f1, f2, null);
				}
			    case OR:{
			    	new Formula<String>(Formula.Content.AND, f1, f2, null);
				}
			    case UNTIL:{
			    	Formula<String> nf1 = new Formula<String>(Formula.Content.AND, f1, f2.negate(), null);
			    	Formula<String> nf2 = new Formula<String>(Formula.Content.AND, f1.negate(), f2.negate(), null);
			    	return new Formula<String>(Formula.Content.WEAK_UNTIL, nf1, nf2, null);
				}
			    case RELEASE:{
			    	Formula<String> nf1 = new Formula<String>(Formula.Content.AND, f1, f2.negate(), null);
			    	Formula<String> nf2 = new Formula<String>(Formula.Content.AND, f1.negate(), f2.negate(), null);
			    	return new Formula<String>(Formula.Content.UNTIL, nf1, nf2, null);
				}
			    case WEAK_UNTIL:{
			    	Formula<String> nf1 = new Formula<String>(Formula.Content.AND, f1, f2.negate(), null);
			    	Formula<String> nf2 = new Formula<String>(Formula.Content.AND, f1.negate(), f2.negate(), null);
			    	return new Formula<String>(Formula.Content.UNTIL, nf1, nf2, null);
				}
			    case NEXT:{
			    	return new Formula<String>(Formula.Content.NEXT, f1, null, null);
				}
			    case NOT: {
			    	return f1;
				}
			    default: return null;
				}
			}
			else if  (f.getContent() == Formula.Content.RELEASE) {
				Formula<String> f1 = toPNF(f.getSub1(),false);
				Formula<String> f2 = toPNF(f.getSub2(),false);
				return new Formula<String>(f.getContent(), f2, f1, null);
			}
			else {
				Formula<String> f1 = toPNF(f.getSub1(),false);
				Formula<String> f2 = null;
				if (f.getSub2()!=null){
					f2 = toPNF(f.getSub2(),false);
				}
				return new Formula<String>(f.getContent(), f1, f2, null);
			}
		}
	}
	
	private HashMap<Formula<?>, Integer> ids = new HashMap<Formula<?>, Integer>();
	int id = 0;
	public int getID (Formula<String> f){
		if (f==null) return -1;
		/*if(ids.containsKey(f))
			return ids.get(f);
		else {*/
			ids.put(f,id);
			id++;
			return id-1;
//		}
			
	}
	
	public void toDCNF(){
		int ltl_id = getID(this.ltl);
		toDCNF(this.ltl,ltl_id);
	}
	
	public void toDCNF(Formula<String> f, int f_id ){

		Formula<String> x = Formula.Proposition("x_"+f_id);
		
		if (f.isLiteral()){
			insertDCNFFormula(f_id,Formula.Iff(x,f));
		}
		else {
			Formula<String> f1 = f.getSub1();
			int f1_id = getID(f1);
			Formula<String> f2 = f.getSub2();
			int f2_id = getID(f2);
			Formula<String> x1 = Formula.Proposition("x_"+f1_id);
			Formula<String> xf = null;
			//unary operator
			if (f2==null) {
				xf = new Formula<String>(f.getContent(), x1, null, null);
			}
			else {
				Formula<String> x2 = Formula.Proposition("x_"+f2_id);
				xf = new Formula<String>(f.getContent(), x1, x2, null);
			}
//			System.out.println(f+" "+getID(f));
			insertDCNFFormula(f_id,Formula.Iff(x,xf));
			toDCNF(f1,f1_id);
			if (f2!=null) {
				toDCNF(f2, f2_id);
			}
		}
	}
	
	private void insertDCNFFormula(int index, Formula<String> f){
		while (dcnf.size()<=index) {
			Formula<String> fs = Formula.True();
			dcnf.add(fs);
		}
		dcnf.set(index, f);
		
	}
	
	public Formula<String> dCNF(){
		if (ltl==null)
			return null;
		if (dcnf.isEmpty()) 
			toDCNF();
		
		Formula<String> x = Formula.Proposition("x_0");
		boolean first = true;
		Formula<String> form = null; 
		for(Formula<String> f : dcnf){
			if (first){
				form = f;
				first = false;
			}
			else {
				form = Formula.And(form, f);
			}
		}
		return Formula.And(x,Formula.Always(form));
	}
	
	
	public Formula<String> getLTL(){
		return ltl;
	}
	
	public List<Formula<String>> getDCNF(){
		return dcnf;
	}
	
	public String toString(){
		return formula.toString();
	}
}
