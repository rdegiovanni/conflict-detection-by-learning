DOM: [[] ( X(open) -> atfloor)]
GOALS: [[](call -> <>(open))]
Initial population size: 22
Total population size: 100

GENERATION 1/50
....m.<;....m.<;....m.<;....m.<;....m.<;....m.<;....m.<;....m.<;....m.<;...i..<;....m.<;...i.m.<;....m.<;....m.<;....m.<;...i.m.;....m.<;...i..<;......m.;.......m.<;....m.<;....m.;....m.<;....m.<;....m.<;....m.<;....m.;....m.<;....m.<;....m.<;....m.<;
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (1): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :0
CALLs: 160    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 16 OF 51

GENERATION 2/50
...i.m.<;....m.<;......m.<;....m.<;....m.<;....m.<;....m.;....m.<;........m.<;......m.<;....m.<;....m.<;....m.<;....m.<;....m.<;......m.<;....m.;....m.<;......m.<;....m.<;....m.<;.....m.;....m.<;......m.;....m.<;........m.<;....m.<;.....m.;....m.<;....m.;....m.<;....m.;....m.;
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (2): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :0
CALLs: 185    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 59 OF 141

GENERATION 3/50
....m.<;....m.<;....m.;.....m.<;.....m.<;....m.;....m.;....m.<;....m.<;......m.<;....m.<;....m.<;....m.<;....m.<;....m.<;....m.<;....m.<;....m.<;....m.<;....m.<;.......m.<;....m.<;....m.<;....m.<;....m.<;....m.<;....m.<;.....m.<;....m.<;....m.<;....m.<;....m.<;....m.<;...i.m.<;....m.<;.....m.<;.....m.;....m.<;....m.<;.....i.m.;....m.<;
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (2): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :0
CALLs: 217    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 105 OF 237

GENERATION 4/50
.....m.<;....m.;....m.<;....m.;....m.<;....m.<;......m.<;........m.<;....m.<;.......m.<;....m.<;........m.<;......m.;....m.<;....m.;....m.<;....m.<;....m.<;.....m.<;.....m.<;....m.<;....m.<;....m.<;......m.<;....m.<;....m.<;....m.<;
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (2): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :0
CALLs: 155    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 145 OF 317

GENERATION 5/50
....m.<;....m.<;....m.<;.....m.<;....m.<;....m.<;.....m.<;....m.<;....m.<;....m.<;....m.<;....m.;.....m.<;....m.<;....m.<;......m.<;....m.<;....m.<;....m.<;.......m.<;....m.;....m.<;....m.<;....m.<;....m.<;.....m.<;....m.<;....m.<;....m.<;....m.<;....m.<;.....m.<;....m.<;....m.;....m.<;....m.<;
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (2): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :1
CALLs: 190    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 194 OF 410

GENERATION 6/50
.....m.<;......m.<;......m.<;....m.<;....m.<;.....m.;....m.<;....m.;....m.;....m.<;....m.;....m.<;....m.<;....m.<;....m.<;....m.<;......m.<;.........m.<;......m.<;......m.<;....m.<;....m.<;....m.<;....m.<;.....m.;....m.<;....m.<;....m.<;....m.<;
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (2): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :1
CALLs: 163    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 237 OF 493

GENERATION 7/50
....m.<;....m.<;....m.;....m.<;....m.<;....m.<;....m.<;....m.<;....m.<;....m.<;....m.<;....m.<;....m.<;....m.<;....m.<;....m.<;.....m.<;....m.<;....m.<;....m.<;....m.<;....m.<;....m.<;....m.<;....m.<;....m.<;.......m.<;....m.<;....m.<;....m.<;......m.<;.....m.<;...i.m.<;....m.<;....m.<;.
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (3): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( ! ( open ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :1
CALLs: 183    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 280 OF 578

GENERATION 8/50
.....m.<;......m.<;....m.<;....m.<;....m.<;....m.<;....m.<;....m.<;....m.<;........m.;....m.;....m.;....m.<;.....m.;....m.<;....m.<;....m.<;....m.<;....m.<;....m.<;.....m.<;....m.<;....m.<;....m.<;......m.<;...i.m.<;...i.m.<;....m.<;
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (5): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( ! ( open ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) /\ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :1
CALLs: 151    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 324 OF 658

GENERATION 9/50
....m.<;....m.<;.....m.<;......m.<;....m.<;.....m.<;....m.<;.....m.<;....m.<;....m.<;...i.m.<;......m.<;....m.<;....m.<;....m.<;....m.<;....m.<;....m.<;....m.<;....m.;....m.<;.....m.<;...i.m.<;
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (5): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( ! ( open ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) /\ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :1
CALLs: 123    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 375 OF 738

GENERATION 10/50
....m.<;.....m.<;....m.<;.....m.<;....m.<;....m.<;.....m.<;....m.<;....m.<;....m.<;....m.<;....m.<;....m.<;....m.<;...i.m.<;
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (6): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( ! ( open ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) /\ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) \/ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :1
CALLs: 78    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 438 OF 819

GENERATION 11/50
.....m.<;....m.<;....m.<;......m.<;....m.<;....m.;....m.<;......i.m.<;
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (6): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( ! ( open ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) /\ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) \/ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :1
CALLs: 46    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 501 OF 895

GENERATION 12/50
......m.<;.......m.<;......m.<;......m.<;....m.<;.....m.<;....m.<;....m.;....m.;...i.m.<;
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (6): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( ! ( open ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) /\ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) \/ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :2
CALLs: 60    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 564 OF 975

GENERATION 13/50
....m.<;.......m.<;....m.<;....m.;....m.<;....m.<;.........m.<;.......m.<;....m.<;....m.<;....m.<;
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (6): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( ! ( open ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) /\ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) \/ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :2
CALLs: 66    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 629 OF 1058

GENERATION 14/50
....m.<;....m.<;.....m.<;....m.<;..
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (6): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( ! ( open ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) /\ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) \/ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :2
CALLs: 23    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 700 OF 1135

GENERATION 15/50
....m.<;......m.<;....m.<;....m.<;....m.<;....m.<;....m.<;....m.<;......m.<;
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (6): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( ! ( open ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) /\ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) \/ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :2
CALLs: 49    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 771 OF 1219

GENERATION 16/50
.....m.<;......m.<;....m.;....m.<;....m.<;......m.<;....m.<;....m.<;....m.<;......m.<;.....m.<;....m.<;....m.<;....m.<;
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (6): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( ! ( open ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) /\ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) \/ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :2
CALLs: 78    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 834 OF 1301

GENERATION 17/50
....m.;......m.<;....m.<;....m.<;....m.<;....m.<;.........
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (6): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( ! ( open ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) /\ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) \/ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :2
CALLs: 41    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 897 OF 1376

GENERATION 18/50
....m.<;....m.;....m.<;....m.<;.....m.;........m.<;......m.<;
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (6): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( ! ( open ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) /\ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) \/ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :2
CALLs: 42    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 957 OF 1447

GENERATION 19/50
....m.<;....m.<;....m.<;....m.<;.....m.<;...i.m.<;
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (6): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( ! ( open ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) /\ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) \/ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :2
CALLs: 31    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 1037 OF 1534

GENERATION 20/50
....m.<;....m.<;....m.<;....m.;....m.<;....m.<;......m.<;........i.m.<;
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (6): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( ! ( open ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) /\ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) \/ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :2
CALLs: 47    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 1104 OF 1613

GENERATION 21/50
....m.<;....m.<;....m.<;....m.<;....m.<;
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (6): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( ! ( open ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) /\ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) \/ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :2
CALLs: 25    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 1178 OF 1692

GENERATION 22/50
....m.<;......
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (6): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( ! ( open ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) /\ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) \/ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :2
CALLs: 11    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 1254 OF 1773

GENERATION 23/50
....m.<;.........m.<;.....m.<;
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (6): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( ! ( open ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) /\ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) \/ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :2
CALLs: 21    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 1323 OF 1850

GENERATION 24/50
......m.;....m.<;......m.<;......m.<;....m.<;
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (6): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( ! ( open ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) /\ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) \/ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :2
CALLs: 31    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 1397 OF 1932

GENERATION 25/50
....m.<;.....m.<;....m.<;.....m.;....m.<;....m.<;....m.<;....m.<;....m.<;
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (6): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( ! ( open ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) /\ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) \/ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :2
CALLs: 47    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 1470 OF 2016

GENERATION 26/50
....m.<;....m.<;....m.<;
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (6): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( ! ( open ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) /\ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) \/ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :2
CALLs: 15    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 1544 OF 2093

GENERATION 27/50
....m.<;....m.<;........m.<;....m.<;......m.<;
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (6): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( ! ( open ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) /\ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) \/ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :2
CALLs: 31    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 1622 OF 2179

GENERATION 28/50
....m.<;.......m.<;
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (6): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( ! ( open ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) /\ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) \/ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :2
CALLs: 13    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 1712 OF 2273

GENERATION 29/50
....m.<;......m.<;
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (6): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( ! ( open ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) /\ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) \/ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :3
CALLs: 12    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 1788 OF 2352

GENERATION 30/50
....m.;....m.<;....m.<;......m.<;
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (6): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( ! ( open ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) /\ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) \/ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :3
CALLs: 22    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 1867 OF 2436

GENERATION 31/50
.....m.<;....m.<;
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (6): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( ! ( open ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) /\ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) \/ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :3
CALLs: 11    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 1949 OF 2521

GENERATION 32/50
....m.<;....m.<;
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (6): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( ! ( open ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) /\ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) \/ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :3
CALLs: 10    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 2021 OF 2595

GENERATION 33/50
....m.<;....m.<;....m.<;....m.<;....
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (6): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( ! ( open ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) /\ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) \/ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :3
CALLs: 24    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 2101 OF 2681

GENERATION 34/50
....m.<;....m.<;....m.<;......m.<;
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (6): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( ! ( open ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) /\ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) \/ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :3
CALLs: 22    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 2173 OF 2758

GENERATION 35/50
....m.<;....m.<;......m.<;....m.<;.....i.m.<;...i.m.<;
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (6): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( ! ( open ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) /\ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) \/ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :3
CALLs: 34    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 2242 OF 2835

GENERATION 36/50
....m.<;....m.<;...
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (6): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( ! ( open ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) /\ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) \/ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :3
CALLs: 13    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 2323 OF 2920

GENERATION 37/50
....m.<;......m.;....m.<;
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (6): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( ! ( open ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) /\ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) \/ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :3
CALLs: 17    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 2402 OF 3003

GENERATION 38/50
......m.<;....m.<;.
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (6): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( ! ( open ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) /\ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) \/ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :3
CALLs: 13    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 2476 OF 3081

GENERATION 39/50
........m.<;..
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (6): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( ! ( open ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) /\ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) \/ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :3
CALLs: 11    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 2554 OF 3164

GENERATION 40/50
....m.<;......m.<;
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (6): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( ! ( open ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) /\ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) \/ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :3
CALLs: 12    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 2629 OF 3242

GENERATION 41/50
....m.<;
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (6): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( ! ( open ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) /\ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) \/ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :3
CALLs: 5    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 2709 OF 3323

GENERATION 42/50
....m.<;......m.<;
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (6): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( ! ( open ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) /\ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) \/ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :3
CALLs: 12    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 2785 OF 3402

GENERATION 43/50
....
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (6): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( ! ( open ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) /\ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) \/ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :3
CALLs: 4    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 2871 OF 3490

GENERATION 44/50
....m.<;.
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (6): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( ! ( open ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) /\ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) \/ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :3
CALLs: 6    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 2947 OF 3568

GENERATION 45/50
....m.<;
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (6): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( ! ( open ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) /\ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) \/ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :3
CALLs: 5    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 3033 OF 3655

GENERATION 46/50
....m.<;....m.;.....m.<;....m.;....m.;
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (6): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( ! ( open ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) /\ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) \/ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :3
CALLs: 26    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 3108 OF 3736

GENERATION 47/50
.......m.<;
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (6): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( ! ( open ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) /\ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) \/ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :3
CALLs: 8    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 3192 OF 3824

GENERATION 48/50
....m.<;....m.<;.....m.<;....m.<;
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (6): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( ! ( open ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) /\ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) \/ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :3
CALLs: 21    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 3271 OF 3908

GENERATION 49/50
....m.<;....m.<;....m.<;....m.<;....m.<;....m.;....m.<;
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (6): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( ! ( open ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) /\ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) \/ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :3
CALLs: 35    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 3347 OF 3991

GENERATION 50/50
......m.<;....m.<;....m.<;
Fittest value: 2.75 for chromosome ( ( call ) /\ ( [] ( ! ( open ) ) ) )
Solutions (6): ( ( call ) /\ ( [] ( ! ( open ) ) ) ) | ( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( ! ( open ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) /\ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( [] ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | ( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) \/ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) ) | 
Generation Time :0
Accumulated Learning Time :3
CALLs: 17    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 3427 OF 4075


TOTAL Learing Time :3

Solutions: 6
( ( call ) /\ ( [] ( ! ( open ) ) ) )
( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) )
( [] ( ( call ) /\ ( ! ( open ) ) ) )
( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) /\ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) )
( [] ( ( call ) /\ ( [] ( ! ( open ) ) ) ) )
( ( ( call ) /\ ( [] ( ! ( open ) ) ) ) \/ ( ( call ) /\ ( [] ( ! ( open ) ) ) ) )

...............
Non Equivalent Solutions: 3
( ( call ) /\ ( [] ( ! ( open ) ) ) )
( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) )
( [] ( ( call ) /\ ( ! ( open ) ) ) )
.....
Weakest Solutions: 2
( ( call ) /\ ( [] ( ! ( open ) ) ) )
( X ( ( call ) /\ ( [] ( ! ( open ) ) ) ) )
TOTAL Time :3
CALLs: 2642    TIMEOUTs: 0    ERRORs: 0
CONFIGURATION
#Population: 100    #Iterations: 50    #ChromSize: 20
REEVALUATIONS: 3427 OF 4075
