#!/bin/bash
java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-d=!b1 && !b2' '-d=[]((b1 && f1) -> X(!b1))' '-d=[]((b2 && f2) -> X(!b2))' '-d=[]((b1 && !f1) -> X(b1))' '-d=[]((b2 && !f2) -> X(b2))' '-g=f1 && !f2' '-g=[](!(f1 && f2))' '-g=[](f1 -> X (f1 || f2))' '-g=[](f2 -> X (f1 || f2))' '-g=[]((f1 && X(f2)) -> X (b1 || b2))' '-g=[]<>(b1 -> f1)' '-g=[]<>(b2 -> f2)' '-g=[]<>(f1)' '-g=[]<>(f2)' > case-studies/liftcontroller/liftcontroller-twofloors.txt

