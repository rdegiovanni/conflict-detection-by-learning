Computing potential Conflicts
Specification Successfully Parsed (2 formulas).
Tableaux .. (#nodes= 30, #trans= 66) done.
Tableau generation time: 0.017113s
Refining tableaux .. (#nodes= 20, #trans= 40) done.
Tableu refinament time: 0.211496s
Computing REACH conflicts ...
#REACH-conflicts= 0
Computing RESPONSE conflicts ...
#RESPONSE-conflicts= 0
Computing SAFETY conflicts ...
#SAFE-conflicts= 1
F (((!p && (q && !r)) || (p && ((!q && !s) || (q && !r)))))
Potential Conflicts time: 0.00046s
All the conflicts have been written.
Total Time: 0.229907s

Filtering: F (((~p & (q & ~r)) | (p & ((~q & ~s) | (q & ~r)))))
F (((~p & (q & ~r)) | (p & ((~q & ~s) | (q & ~r))))) & G ((p -> (G (q) | (q U s)))) & G ((q -> r))
please input the formula:
unsat
F (((~p & (q & ~r)) | (p & ((~q & ~s) | (q & ~r))))) & G ((p -> (G (q) | (q U s))))
please input the formula:
sat
F (((~p & (q & ~r)) | (p & ((~q & ~s) | (q & ~r))))) & G ((q -> r))
please input the formula:
sat

#BCs: 1
Computed BCs:
F (((~p & (q & ~r)) | (p & ((~q & ~s) | (q & ~r)))))

TOTAL execution time (secs) 0
