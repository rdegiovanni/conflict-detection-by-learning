#!/bin/bash
java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=20' > case-studies/tcp/sensivitypopsize/exec0-pop20.txt
java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=20' > case-studies/tcp/sensivitypopsize/exec1-pop20.txt
java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=20' > case-studies/tcp/sensivitypopsize/exec2-pop20.txt
java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=20' > case-studies/tcp/sensivitypopsize/exec3-pop20.txt
java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=20' > case-studies/tcp/sensivitypopsize/exec4-pop20.txt
java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=20' > case-studies/tcp/sensivitypopsize/exec5-pop20.txt
java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=20' > case-studies/tcp/sensivitypopsize/exec6-pop20.txt
java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=20' > case-studies/tcp/sensivitypopsize/exec7-pop20.txt
java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=20' > case-studies/tcp/sensivitypopsize/exec8-pop20.txt
java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=20' > case-studies/tcp/sensivitypopsize/exec9-pop20.txt

java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=40' > case-studies/tcp/sensivitypopsize/exec0-pop40.txt
java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=40' > case-studies/tcp/sensivitypopsize/exec1-pop40.txt
java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=40' > case-studies/tcp/sensivitypopsize/exec2-pop40.txt
java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=40' > case-studies/tcp/sensivitypopsize/exec3-pop40.txt
java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=40' > case-studies/tcp/sensivitypopsize/exec4-pop40.txt
java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=40' > case-studies/tcp/sensivitypopsize/exec5-pop40.txt
java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=40' > case-studies/tcp/sensivitypopsize/exec6-pop40.txt
java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=40' > case-studies/tcp/sensivitypopsize/exec7-pop40.txt
java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=40' > case-studies/tcp/sensivitypopsize/exec8-pop40.txt
java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=40' > case-studies/tcp/sensivitypopsize/exec9-pop40.txt

java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=60' > case-studies/tcp/sensivitypopsize/exec0-pop60.txt
java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=60' > case-studies/tcp/sensivitypopsize/exec1-pop60.txt
java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=60' > case-studies/tcp/sensivitypopsize/exec2-pop60.txt
java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=60' > case-studies/tcp/sensivitypopsize/exec3-pop60.txt
java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=60' > case-studies/tcp/sensivitypopsize/exec4-pop60.txt
java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=60' > case-studies/tcp/sensivitypopsize/exec5-pop60.txt
java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=60' > case-studies/tcp/sensivitypopsize/exec6-pop60.txt
java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=60' > case-studies/tcp/sensivitypopsize/exec7-pop60.txt
java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=60' > case-studies/tcp/sensivitypopsize/exec8-pop60.txt
java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=60' > case-studies/tcp/sensivitypopsize/exec9-pop60.txt

java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=80' > case-studies/tcp/sensivitypopsize/exec0-pop80.txt
java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=80' > case-studies/tcp/sensivitypopsize/exec1-pop80.txt
java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=80' > case-studies/tcp/sensivitypopsize/exec2-pop80.txt
java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=80' > case-studies/tcp/sensivitypopsize/exec3-pop80.txt
java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=80' > case-studies/tcp/sensivitypopsize/exec4-pop80.txt
java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=80' > case-studies/tcp/sensivitypopsize/exec5-pop80.txt
java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=80' > case-studies/tcp/sensivitypopsize/exec6-pop80.txt
java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=80' > case-studies/tcp/sensivitypopsize/exec7-pop80.txt
java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=80' > case-studies/tcp/sensivitypopsize/exec8-pop80.txt
java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=80' > case-studies/tcp/sensivitypopsize/exec9-pop80.txt

java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=100' > case-studies/tcp/sensivitypopsize/exec0-pop100.txt
java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=100' > case-studies/tcp/sensivitypopsize/exec1-pop100.txt
java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=100' > case-studies/tcp/sensivitypopsize/exec2-pop100.txt
java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=100' > case-studies/tcp/sensivitypopsize/exec3-pop100.txt
java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=100' > case-studies/tcp/sensivitypopsize/exec4-pop100.txt
java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=100' > case-studies/tcp/sensivitypopsize/exec5-pop100.txt
java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=100' > case-studies/tcp/sensivitypopsize/exec6-pop100.txt
java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=100' > case-studies/tcp/sensivitypopsize/exec7-pop100.txt
java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=100' > case-studies/tcp/sensivitypopsize/exec8-pop100.txt
java -Xmx8g -Djava.library.path=/usr/local/lib -cp bin/.:lib/* main.Main '-g=[](send -> (!ack U delivered))' '-g=[] (delivered -> (!send U ack))' '-ps=100' > case-studies/tcp/sensivitypopsize/exec9-pop100.txt
