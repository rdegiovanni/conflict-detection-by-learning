DOM: [[] (<> (! r_m))]
GOALS: [[] (r_m -> X ((! g_0 && ! g_1) U g_m)), [] (! g_0 && true || (true && (! g_1))), [] (! (g_m && g_0)), [] (! (g_m && g_1)), [] (r_0 -> <> g_0) && [] (r_1 -> <> g_1), [] (r_0 && X r_1 -> X (X (g_0 && g_1)))]
Initial population size: 84
Total population size: 100

GENERATION 1/50
....m.m.m.m.m.m.<;...i.......<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i.....m..<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i....m...<;....m.m.m.m.m.m.<;...i....m...<;....m.m.m.m.m.m.<;...i.m.m.m....<;....m.m.m.m.m.m.<;...i....m...<;....m.m.m.m.m.m.<;...i......m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i....m...<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i...m....<;....m.m.m.m.m.m.<;...i.m......<;....m.m.m.m.m.m.<;...i.m.m.m....<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i.....m..<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i...m....<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i.....m..<;...i.....m..<;....m.m.m.m.m.m.<;.......m.m.m.m.m.m.<;...i.....m.m.<;....m.m.m.m.m.m.<;...i.......<;....m.m.m.m.m.m.<;...i......m.<;....m.m.m.m.m.m.<;...i....m...<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i.m......<;...i.....m..<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;.....m.m.m.m.m.m.<;.....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i.....m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i...m....<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;......m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i.....m..<;....m.m.m.m.m.m.<;.....i.....m.m.<;...i.....m.m.<;...i....m...<;......m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i.....m..<;....m.m.m.m.m.m.<;
Best fitness value: ( ( r_m ) /\ ( X ( ( ( g_0 ) \/ ( g_1 ) ) V ( ( g_0 ) \/ ( g_1 ) ) ) ) ) 2.0
Generation Time :1
Accumulated Learning Time :1
CALLs: 1081    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 44 OF 158

GENERATION 2/50
...i.m.m.m....<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i....m...<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;.........i.....m.m.<;...i....m...<;......m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;......m.m.m.m.m.m.<;.....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;......m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;......m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;.....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;.....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i.....m.m.<;....m.m.m.m.m.m.<;...i.....m..<;
Best fitness value: ( ( r_m ) /\ ( X ( ( ( g_0 ) U ( g_1 ) ) V ( ( g_0 ) U ( g_1 ) ) ) ) ) 2.0
Generation Time :0
Accumulated Learning Time :2
CALLs: 467    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 71 OF 241

GENERATION 3/50
....m.m.m.m.m.m.<;.....m.m.m.m.m.m.<;...i..m.m....<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i.....m.m.<;....m.m.m.m.m.m.<;...i.....m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i....m...<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i.....m..<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i.....m.m.<;......m.m.m.m.m.m.<;.......m.m.m.m.m.m.<;...i.......<;....m.m.m.m.m.m.<;.......m.m.m.m.m.m.<;.....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;.....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;.....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;......m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;.....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i.m.m.m....<;...i.m......<;...i.....m..<;...i.....m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i.m......<;
Best fitness value: ( ( r_m ) /\ ( X ( ( ( g_0 ) \/ ( g_1 ) ) V ( ( g_0 ) \/ ( g_1 ) ) ) ) ) 2.0
Generation Time :0
Accumulated Learning Time :3
CALLs: 565    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 89 OF 326

GENERATION 4/50
...i.m.m.m....<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i.....m..<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i.....m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i.....m..<;....i....m...<;...i....m...<;....m.m.m.m.m.m.<;...i...m....<;....m.m.m.m.m.m.<;...i....m...<;...i.....m..<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i....m.m..<;.....m.m.m.m.m.m.<;.....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;.....m.m.m.m.m.m.<;.....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;......m.m.m.m.m.m.<;....m.m.m.m.m.m.<;.....m.m.m.m.m.m.<;...i.....m..<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;.....m.m.m.m.m.m.<;...i..m.m....<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i.m.m.m....<;...i....m...<;...i....m...<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i.m.m.m....<;
Best fitness value: ( ( r_m ) /\ ( X ( ( ( g_0 ) U ( g_1 ) ) V ( ( g_0 ) U ( g_1 ) ) ) ) ) 2.0
Generation Time :0
Accumulated Learning Time :4
CALLs: 619    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 95 OF 401

GENERATION 5/50
...i.m.m.m....<;....m.m.m.m.m.m.<;.....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i.m.m.....<;...i.......<;....m.m.m.m.m.m.<;.....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i.......<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i.m.m.m....<;...i.m.m.....<;...i..m.m....<;...i..m.m....<;...i.....m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i.....m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i.....m..<;...i.....m.m.<;....m.m.m.m.m.m.<;...i.....m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;.....m.m.m.m.m.m.<;....i....m...<;......m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i.m......<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;......m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i....m...<;...i.....m..<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;.....m.m.m.m.m.m.<;...i.m.m.m....<;...i....m.m..<;....m.m.m.m.m.m.<;...i.....m.m.<;...i.....m.m.<;...i.m.m.m....<;...i.m......<;
Best fitness value: ( ( r_m ) /\ ( X ( ( ( g_0 ) \/ ( g_1 ) ) V ( ( g_0 ) \/ ( g_1 ) ) ) ) ) 2.0
Generation Time :1
Accumulated Learning Time :5
CALLs: 709    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 99 OF 483

GENERATION 6/50
...i.m.m.m....<;...i.m.m.m....<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;.....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i..m.m....<;....m.m.m.m.m.m.<;...i.m.m.m....<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i.m.m.....<;....m.m.m.m.m.m.<;...i.....m.m.<;......m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i.....m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i.....m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i....m...<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;......m.m.m.m.m.m.<;....i.m.m.m....<;...i.m.m.....<;...i..m.m....<;...i.m.m.m....<;...i..m.m....<;....m.m.m.m.m.m.<;.....m.m.m.m.m.m.<;...i.......<;...i.m.m.m....<;...i.m.m.m....<;...i.....m.m.<;...i.....m.m.<;...i.m.m.m....<;
Best fitness value: ( ( r_m ) /\ ( X ( ( ( g_0 ) U ( g_1 ) ) V ( ( g_0 ) U ( g_1 ) ) ) ) ) 2.0
Generation Time :1
Accumulated Learning Time :6
CALLs: 637    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 108 OF 561

GENERATION 7/50
....m.m.m.m.m.m.<;...i.......<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i.m.m.....<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;.....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i..m.m....<;.....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i..m.m....<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i.m.m.m....<;.....m.m.m.m.m.m.<;...i..m.....<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i.....m.m.<;...i.....m.m.<;...i..m.m....<;...i.....m..<;....m.m.m.m.m.m.<;...i.m.m.....<;....m.m.m.m.m.m.<;....i.....m.m.<;...i.....m..<;...i....m.m..<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i.m.m.m....<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;.....i.m.m.m....<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i.....m..<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i.m.m.m....<;...i.......<;....m.m.m.m.m.m.<;
Best fitness value: ( ( r_m ) /\ ( X ( ( ( g_0 ) \/ ( g_1 ) ) V ( ( g_0 ) \/ ( g_1 ) ) ) ) ) 2.0
Generation Time :2
Accumulated Learning Time :8
CALLs: 686    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 116 OF 642

GENERATION 8/50
....m.m.m.m.m.m.<;...i.m.m.m....<;....m.m.m.m.m.m.<;.....m.m.m.m.m.m.<;...i.m.m.....<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i.m.m.....<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i.m.m.m....<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;......m.m.m.m.m.m.<;...i.m.m.m....<;....i.m.m.m....<;....m.m.m.m.m.m.<;...i.m.m.m....<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i..m.m....<;....m.m.m.m.m.m.<;...i.m.m.m....<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i..m.m....<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i.m.m.....<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i..m.m....<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i..m.m....<;....m.m.m.m.m.m.<;...i.m.m.m....<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i.m.m.m....<;....m.m.m.m.m.m.<;...i.m.m.m....<;...i.m.m.m....<;
Best fitness value: ( ( r_m ) /\ ( X ( ( ( g_0 ) U ( g_1 ) ) V ( ( g_0 ) U ( g_1 ) ) ) ) ) 2.0
Generation Time :1
Accumulated Learning Time :10
CALLs: 624    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 128 OF 719

GENERATION 9/50
......m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i.m.m.m....<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i.m.m.....<;....m.m.m.m.m.m.<;...i.......<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i.m.m.m....<;....m.m.m.m.m.m.<;.....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i.m.m.m....<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;.....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;.....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i.m.m.m....<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i..m.m....<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i..m.m....<;...i.......<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i.m.m.m....<;...i.m.m.....<;....m.m.m.m.m.m.<;....m.m.m.m.m.m.<;...i.m.m.m....<;...i.m.m.m....<;....m.m.m.m.m.m.<;.....m.m.m.m.m.m.<;.....i.m.m.m....<;...i.m.m.m....<;...i.m.m.m....<;...i.m.m.m....<;....m.m.m.m.m.m.<;...i.m.m.m....<;....m.m.m.m.m.m.<;...i..m.m....<;
Best fitness value: ( ( r_m ) /\ ( X ( ( ( g_0 ) \/ ( g_1 ) ) V ( ( g_0 ) \/ ( g_1 ) ) ) ) ) 2.0
Generation Time :1
Accumulated Learning Time :11
CALLs: 578    TIMEOUTs: 0    ERRORs: 0
REEVALUATIONS: 149 OF 803

GENERATION 10/50

TOTAL Learing Time :14

Solutions: 0


Non Equivalent Solutions: 0

Weakest Solutions: 0
TOTAL Time :14
CALLs: 5966    TIMEOUTs: 0    ERRORs: 0
CONFIGURATION
#Population: 100    #Iterations: 50    #ChromSize: 50
REEVALUATIONS: 149 OF 803
