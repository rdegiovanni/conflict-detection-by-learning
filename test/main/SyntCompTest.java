package main;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.jgap.InvalidConfigurationException;
import org.junit.Test;

import gov.nasa.ltl.trans.ParseErrorException;

public class SyntCompTest {

	@Test
	public void testSimpleArbiter() throws InvalidConfigurationException, ParseErrorException {
		Set<String> dom = new HashSet<>();
		Set<String> goals = new HashSet<>();
		
		String d0 = "!r1";
		dom.add(d0);
		String d1 = "!r2";
		dom.add(d1);
		String d2 = "[]((((!r1)&&(g1))||((r1)&&(!g1)))->(((!r1)&&((X(!r1))))||((r1)&&((X(r1))))))";
		dom.add(d2);
		String d3 = "[]((((!r2)&&(g2))||((r2)&&(!g2)))->(((!r2)&&((X(!r2))))||((r2)&&((X(r2))))))";
		dom.add(d3);
		String d4 = "[](<>(!((r1)&&(g1))))";
		dom.add(d4);
		String d5 = "[](<>(!((r2)&&(g2))))";
		dom.add(d5);
		String g0 = "!g1";
		goals.add(g0);
		String g1 = "!g2";
		goals.add(g1);
		String g2 = "[](!(((X(g1)))&&((X(g2)))))";
		goals.add(g2);
		String g3 = "[]((((!r1)&&(!g1))||((r1)&&(g1)))->(((!g1)&&((X(!g1))))||((g1)&&((X(g1))))))";
		goals.add(g3);
		String g4 = "[]((((!r2)&&(!g2))||((r2)&&(g2)))->(((!g2)&&((X(!g2))))||((g2)&&((X(g2))))))";
		goals.add(g4);
		String g5 = "[](<>(((!r1)&&(!g1))||((r1)&&(g1))))";
		goals.add(g5);
		String g6 = "[](<>(((!r2)&&(!g2))||((r2)&&(g2))))";
		goals.add(g6);
		
//		
//		System.out.println("DOM: "+dom);
//		System.out.println("GOALS: "+goals);
		
		String cmd = "";
		for (String s: dom)
			cmd += "'-d="+s+"' ";
		for (String g: goals)
			cmd += "'-g="+g+"' ";
		System.out.println(cmd);
		
		BCLearner learner = new BCLearner(dom, goals);
		learner.learnBC();
	}
	
	@Test
	public void test_simple_arbiter_unreal1() throws InvalidConfigurationException, ParseErrorException {
		// syntcomp/Benchmarks2017/TLSF/parameterized_variants/simple_arbiter_unreal1.tlsf 
		Set<String> dom = new HashSet<>();
		Set<String> goals = new HashSet<>();
		
		String d1 = "[] (! (g_0 || g_1)  -> (! g_0 && true || (true && (! g_1)) && (! g_2)))";
		String d2 = "[] (! (g_0 || g_1)  -> (r_0 && X r_1 -> X (X (X (g_0 && g_1)))))";
		String d3 = "[] (! (g_0 || g_1)  -> (r_0 && X r_2 -> X (X (X (g_0 && g_2)))))";
		String d4 = "[] (! (g_0 || g_1)  -> (r_1 && X r_2 -> X (X (X (g_1 && g_2)))))";
		dom.add(d1);
		dom.add(d2);
		dom.add(d3);
		dom.add(d4);
		
		String g1 = "[] (r_0 -> <> g_0)";
		String g2 = "[] (r_1 -> <> g_1) ";
		String g3 = "[] (r_2 -> <> g_2)";
		goals.add(g1);
		goals.add(g2);
		goals.add(g3);
		
		System.out.println("DOM: "+dom);
		System.out.println("GOALS: "+goals);
		
//		String cmd = "";
//		for (String s: dom)
//			cmd += "'-d="+s+"' ";
//		for (String g: goals)
//			cmd += "'-g="+g+"' ";
//		System.out.println(cmd);
		
		BCLearner learner = new BCLearner(dom, goals);
		learner.learnBC();
	}
	
	@Test
	public void test_simple_arbiter_unreal2() throws InvalidConfigurationException, ParseErrorException {
		// syntcomp/Benchmarks2017/TLSF/parameterized_variants/simple_arbiter_unreal2.tlsf 
		Set<String> dom = new HashSet<>();
		Set<String> goals = new HashSet<>();
		
		String d1 = "[] (! (g_0 || g_1)  -> (! g_0 && true || (true && (! g_1)) && (! g_2)))";
		String d2 = "[] (! (g_0 || g_1)  -> (r_0 && X r_1 -> <> (g_0 && g_1)))";
		String d3 = "[] (! (g_0 || g_1)  -> (r_0 && X r_2 -> <> (g_0 && g_2)))";
		String d4 = "[] (! (g_0 || g_1)  -> (r_1 && X r_2 -> <> (g_1 && g_2))))";
		dom.add(d1);
		dom.add(d2);
		dom.add(d3);
		dom.add(d4);
		
		String g1 = "[] (r_0 -> <> g_0)";
		String g2 = "[] (r_1 -> <> g_1) ";
		String g3 = "[] (r_2 -> <> g_2)";
		goals.add(g1);
		goals.add(g2);
		goals.add(g3);
		
		System.out.println("DOM: "+dom);
		System.out.println("GOALS: "+goals);
		
//		String cmd = "";
//		for (String s: dom)
//			cmd += "'-d="+s+"' ";
//		for (String g: goals)
//			cmd += "'-g="+g+"' ";
//		System.out.println(cmd);
		
		BCLearner learner = new BCLearner(dom, goals);
		learner.learnBC();
	}
	
	
	@Test
	public void test_simple_arbiter_unreal3() throws InvalidConfigurationException, ParseErrorException {
		// syntcomp/Benchmarks2017/TLSF/parameterized_variants/simple_arbiter_unreal3.tlsf 
		Set<String> dom = new HashSet<>();
		Set<String> goals = new HashSet<>();
		String d1 = "[] (! (g_0 || g_1)  -> (! g_0 && true || (true && (! g_1)) && (! g_2)))";
		String d2 = "[] (! (g_0 || g_1)  -> (r_0 && X r_1 -> <> (g_0 && g_1)))";
		String d3 = "[] (! (g_0 || g_1)  -> (r_0 && X r_2 -> <> (g_0 && g_2)))";
		String d4 = "[] (! (g_0 || g_1)  -> (r_1 && X r_2 -> <> (g_1 && g_2))))";
		dom.add(d1);
		dom.add(d2);
		dom.add(d3);
		dom.add(d4);
		
		String g1 = "[] (r_0 -> <> (g_0 && X g_0))";
		String g2 = "[] (r_1 -> <> (g_1 && X g_1))";
		String g3 = "[] (r_2 -> <> (g_2 && X g_2))";
		goals.add(g1);
		goals.add(g2);
		goals.add(g3);
		
		System.out.println("DOM: "+dom);
		System.out.println("GOALS: "+goals);
		
//		String cmd = "";
//		for (String s: dom)
//			cmd += "'-d="+s+"' ";
//		for (String g: goals)
//			cmd += "'-g="+g+"' ";
//		System.out.println(cmd);
		
		BCLearner learner = new BCLearner(dom, goals);
		learner.learnBC();
	}
	
	@Test
	public void test_prioritized_arbiter_unreal1() throws InvalidConfigurationException, ParseErrorException {
		// syntcomp/Benchmarks2017/TLSF/parameterized_variants/prioritized_arbiter_unreal1.tlsf 
		Set<String> dom = new HashSet<>();
		Set<String> goals = new HashSet<>();
		String d1 = "[] (<> (! r_m))";
		dom.add(d1);
		
		String g1 = "[] (! g_0 && true || (true && (! g_1)))";
		String g2 = "[] (! (g_m && g_0))";
		String g3 = "[] (! (g_m && g_1))";
		String g4 = "[] (r_0 && X r_1 -> X (X (g_0 && g_1)))";
		String g5 = "[] (r_0 -> <> g_0) && [] (r_1 -> <> g_1)";
		String g6 = "[] (r_m -> X ((! g_0 && ! g_1) U g_m))";
		goals.add(g1);
		goals.add(g2);
		goals.add(g3);
		goals.add(g4);
		goals.add(g5);
		goals.add(g6);
		
		System.out.println("DOM: "+dom);
		System.out.println("GOALS: "+goals);
		
//		String cmd = "";
//		for (String s: dom)
//			cmd += "'-d="+s+"' ";
//		for (String g: goals)
//			cmd += "'-g="+g+"' ";
//		System.out.println(cmd);
		
		BCLearner learner = new BCLearner(dom, goals);
		learner.learnBC();
	}
	
	
	@Test
	public void test_round_robin_arbiter_unreal1() throws InvalidConfigurationException, ParseErrorException {
		// syntcomp/Benchmarks2017/TLSF/parameterized_variants/round_robin_arbiter_unreal1.tlsf 
		Set<String> dom = new HashSet<>();
		Set<String> goals = new HashSet<>();
		String d1 = "[] (r_0 && ! g_0 -> X r_0)";
		String d2 = "[] (! r_0 && g_0 -> X (! r_0))"; 
		String d3 = "[]<> (! (r_0 && g_0))";
		String d4 = "[] (r_1 && ! g_1 -> X r_1)";
		String d5 = "[] (! r_1 && g_1 -> X (! r_1))"; 
		String d6 = "[]<> (! (r_1 && g_1))";
		dom.add(d1);
		dom.add(d2);
		dom.add(d3);
		dom.add(d4);
		dom.add(d5);
		dom.add(d6);
		
		String g1 = "[] (! g_0 && true || (true && (! g_1)) && (r_0 && X r_1 -> X (X (g_0 && g_1))))";
		String g2 = "[] (r_0 -> <> g_0)";
		String g3 = "[] (r_1 -> <> g_1))";
		goals.add(g1);
		goals.add(g2);
		goals.add(g3);
		System.out.println("DOM: "+dom);
		System.out.println("GOALS: "+goals);
		
//		String cmd = "";
//		for (String s: dom)
//			cmd += "'-d="+s+"' ";
//		for (String g: goals)
//			cmd += "'-g="+g+"' ";
//		System.out.println(cmd);
		
		BCLearner learner = new BCLearner(dom, goals);
		learner.learnBC();
	}
	
	@Test
	public void test_amba_case_study_unreal1() throws InvalidConfigurationException, ParseErrorException {
		// syntcomp/Benchmarks2017/TLSF/parameterized_variants/amba_case_study_unreal1.tlsf 
		Set<String> dom = new HashSet<>();
		Set<String> goals = new HashSet<>();
		String d1 = "[] (! hburst_0 || ! hburst_1)";
		String d2 = "[] (hmastlock && (! hburst_0 && ! hburst_1) -> X (<> (! busreq)))";
		String d3 = "[] (<> hready)";
		String d4 = "[] (hlock_0 -> hbusreq_0)";
		String d5 = "[] (hlock_1 -> hbusreq_1)";
		String d6 = "! hready && ! hbusreq_0 && ! hlock_0 && ! hbusreq_1 && ! hlock_1";
		dom.add(d1);
		dom.add(d2);
		dom.add(d3);
		dom.add(d4);
		dom.add(d5);
		dom.add(d6);
		
		String g1 = "[] (true && ! hmaster_1 && ! hmaster_0 -> (busreq <-> hbusreq_0))"; 
		String g2 = "[] (true && ! hmaster_1 && hmaster_0 -> (busreq <-> hbusreq_1))";
		String g3 = "[] (! hready -> X (! start))";
		String g4 = "[] (hmastlock && (! hburst_0 && ! hburst_1) && start -> X (! start U (! start && ! busreq) || [] (! start)))";
		String g5 = "[](hmastlock && (hburst_0 && ! hburst_1) && start && hready -> X (! start U (! start && hready && X (! start U (! start && hready && X (! start U (! start && hready) || [] (! start))) || [] (! start))) || [] (! start)))";
		String g6 = "[](hmastlock && (hburst_0 && ! hburst_1) && start && ! hready -> X (! start U (! start && hready && X (! start U (! start && hready && X (! start U (! start && hready && X (! start U (! start && hready) || [] (! start))) || [] (! start))) || [] (! start))) || [] (! start)))";
		String g7 = "[](hready -> (hgrant_0 <-> X (true && ! hmaster_1 && ! hmaster_0)))";
		String g8 = "[](hready -> (hgrant_1 <-> X (true && ! hmaster_1 && hmaster_0)))"; 
		String g9 = "[](hready -> (locked <-> X hmastlock))";
		String g10 = "[](X (! start) -> (true && ! hmaster_1 && ! hmaster_0 <-> X (true && ! hmaster_1 && ! hmaster_0) && (hmastlock <-> X hmastlock)))"; 
		String g11 = "[](X (! start) -> (true && ! hmaster_1 && hmaster_0 <-> X (true && ! hmaster_1 && hmaster_0) && (hmastlock <-> X hmastlock)))";
		String g12 = "[](decide && X hgrant_0 -> (hlock_0 <-> X locked))"; 
		String g13 = "[](decide && X hgrant_1 -> (hlock_1 <-> X locked))"; 
		String g14 = "[](! decide -> (hgrant_0 <-> X hgrant_0 && (hgrant_1 <-> X hgrant_1)))";
		String g15 = "[](! decide -> (locked <-> X locked))";
		String g16 = "[](hbusreq_0 -> <> (! hbusreq_0 || (true && ! hmaster_1 && ! hmaster_0)))"; 
		String g17 = "[](hbusreq_1 -> <> (! hbusreq_1 || (true && ! hmaster_1 && hmaster_0)))";
		String g18 = "[](! hgrant_1 -> (! hgrant_1 U hbusreq_1 || [] (! hgrant_1)))"; 
		String g19 = "[](! hbusreq_0 && ! hbusreq_1 && decide -> X hgrant_0)";
		String g20 = "[](hbusreq_0 && X hbusreq_1 -> X (X (hgrant_0 && hgrant_1)))";
		String g21 = "(decide && start && hgrant_0 && true && ! hmaster_1 && ! hmaster_0 && ! hmastlock && ! hgrant_1)";
		goals.add(g1);
		goals.add(g2);
		goals.add(g3);
		goals.add(g4);
		goals.add(g5);
		goals.add(g6);
		goals.add(g7);
		goals.add(g8);
		goals.add(g9);
		goals.add(g10);
		goals.add(g11);
		goals.add(g12);
		goals.add(g13);
		goals.add(g14);
		goals.add(g15);
		goals.add(g16);
		goals.add(g17);
		goals.add(g18);
		goals.add(g19);
		goals.add(g20);
		goals.add(g21);
		System.out.println("DOM: "+dom);
		System.out.println("GOALS: "+goals);
		
//		String cmd = "";
//		for (String s: dom)
//			cmd += "'-d="+s+"' ";
//		for (String g: goals)
//			cmd += "'-g="+g+"' ";
//		System.out.println(cmd);
		
		BCLearner learner = new BCLearner(dom, goals);
		learner.learnBC();
		
	}
	
	@Test
	public void test_load_balancer_unreal1() throws InvalidConfigurationException, ParseErrorException {
		// syntcomp/Benchmarks2017/TLSF/parameterized_variants/load_balancer_unreal1.tlsf
		Set<String> dom = new HashSet<>();
		Set<String> goals = new HashSet<>();
		String d1 = "[] (<> idle)";
		String d2 = "[] (idle && X (! grant_0 && ! grant_1) -> X idle)";
		String d3 = "[] (X (! grant_0) || X ((! request_0 && ! idle) U (! request_0 && idle)))";
		dom.add(d1);
		dom.add(d2);
		dom.add(d3);
		
		String g1 = "[] (X (! grant_0 && true || (true && (! grant_1))))";
		String g2 = "[](X grant_0 -> request_0)";
		String g3 = "[](X grant_1 -> request_1)"; 
		String g4 = "[](request_0 -> grant_1)";
		String g5 = "[](! idle -> X (! grant_0 && ! grant_1))";
		String g6 = "[](request_0 && X request_1 -> X (X (grant_0 && grant_1)))";
		String g7 = "! <> ([] (request_0 && X (! grant_0)))";
		String g8 = "! <> ([] (request_1 && X (! grant_1)))";
		goals.add(g1);
		goals.add(g2);
		goals.add(g3);
		goals.add(g4);
		goals.add(g5);
		goals.add(g6);
		goals.add(g7);
		goals.add(g8);
		
		System.out.println("DOM: "+dom);
		System.out.println("GOALS: "+goals);
		
//		String cmd = "";
//		for (String s: dom)
//			cmd += "'-d="+s+"' ";
//		for (String g: goals)
//			cmd += "'-g="+g+"' ";
//		System.out.println(cmd);
		
		BCLearner learner = new BCLearner(dom, goals);
		learner.learnBC();
	}
	
	@Test
	public void testArbiterExample() throws InvalidConfigurationException, ParseErrorException {
		Set<String> dom = new HashSet<>();
		Set<String> goals = new HashSet<>();
		String d0 = "!ra";
		dom.add(d0);
		String d1 = "!rb";
		dom.add(d1);
		String d2 = "!rc";
		dom.add(d2);
		String d3 = "!rd";
		dom.add(d3);
		String d4 = "!re";
		dom.add(d4);
		String d5 = "!rf";
		dom.add(d5);
//		String d6 = "[]((((ra)&&(!ga)->(X(ra)))&&((!ra)&&(ga)->(X(!ra))))&&(((rb)&&(!gb)->(X(rb)))&&((!rb)&&(gb)->(X(!rb))))&&(((rc)&&(!gc)->(X(rc)))&&((!rc)&&(gc)->(X(!rc))))&&(((rd)&&(!gd)->(X(rd)))&&((!rd)&&(gd)->(X(!rd))))&&(((re)&&(!ge)->(X(re)))&&((!re)&&(ge)->(X(!re))))&&(((rf)&&(!gf)->(X(rf)))&&((!rf)&&(gf)->(X(!rf)))))";
//		dom.add(d6);
		String d6a = "[](((ra)&&(!ga)->(X(ra))) && ((!ra)&&(ga)->(X(!ra))))";
		String d6b = "[](((rb)&&(!gb)->(X(rb))) && ((!rb)&&(gb)->(X(!rb))))";
		String d6c = "[](((rc)&&(!gc)->(X(rc))) && ((!rc)&&(gc)->(X(!rc))))";
		String d6d = "[](((rd)&&(!gd)->(X(rd))) && ((!rd)&&(gd)->(X(!rd))))";
		String d6e = "[](((re)&&(!ge)->(X(re))) && ((!re)&&(ge)->(X(!re))))";
		String d6f = "[](((rf)&&(!gf)->(X(rf))) && ((!rf)&&(gf)->(X(!rf))))";
		dom.add(d6a);dom.add(d6b);dom.add(d6c);dom.add(d6d);dom.add(d6e);dom.add(d6f);
		String d7 = "[](<>((!ra)||(!ga)))";
		dom.add(d7);
		String d8 = "[](<>((!rb)||(!gb)))";
		dom.add(d8);
		String d9 = "[](<>((!rc)||(!gc)))";
		dom.add(d9);
		String d10 = "[](<>((!rd)||(!gd)))";
		dom.add(d10);
		String d11 = "[](<>((!re)||(!ge)))";
		dom.add(d11);
		String d12 = "[](<>((!rf)||(!gf)))";
		dom.add(d12);
		String g0 = "!ga";
		goals.add(g0);
		String g1 = "!gb";
		goals.add(g1);
		String g2 = "!gc";
		goals.add(g2);
		String g3 = "!gd";
		goals.add(g3);
		String g4 = "!ge";
		goals.add(g4);
		String g5 = "!gf";
		goals.add(g5);
//		String g6 = "[]((!((X(ga))&&(X(gb))&&(X(gc))&&(X(gd))&&(X(ge))&&(X(gf))))&&((((!ra)&&(!ga))->(X(!ga)))&&(((ra)&&(ga))->(X(ga))))&&((((!rb)&&(!gb))->(X(!gb)))&&(((rb)&&(gb))->(X(gb))))&&((((!rc)&&(!gc))->(X(!gc)))&&(((rc)&&(gc))->(X(gc))))&&((((!rd)&&(!gd))->(X(!gd)))&&(((rd)&&(gd))->(X(gd))))&&((((!re)&&(!ge))->(X(!ge)))&&(((re)&&(ge))->(X(ge))))&&((((!rf)&&(!gf))->(X(!gf)))&&(((rf)&&(gf))->(X(gf)))))";
//		goals.add(g6);
		String g6 = "[]((!((X(ga))&&(X(gb))&&(X(gc))&&(X(gd))&&(X(ge))&&(X(gf)))))";
		String g6a = "[]((((!ra)&&(!ga))->(X(!ga)))&&(((ra)&&(ga))->(X(ga))))";
		String g6b = "[]((((!rb)&&(!gb))->(X(!gb)))&&(((rb)&&(gb))->(X(gb))))";
		String g6c = "[]((((!rc)&&(!gc))->(X(!gc)))&&(((rc)&&(gc))->(X(gc))))";
		String g6d = "[]((((!rd)&&(!gd))->(X(!gd)))&&(((rd)&&(gd))->(X(gd))))";
		String g6e = "[]((((!re)&&(!ge))->(X(!ge)))&&(((re)&&(ge))->(X(ge))))";
		String g6f = "[]((((!rf)&&(!gf))->(X(!gf)))&&(((rf)&&(gf))->(X(gf))))";
		goals.add(g6);goals.add(g6a);goals.add(g6b);goals.add(g6c);goals.add(g6d);goals.add(g6e);goals.add(g6f);
		String g7 = "[](<>(((ra)&&(ga))||((!ra)&&(!ga))))";
		goals.add(g7);
		String g8 = "[](<>(((rb)&&(gb))||((!rb)&&(!gb))))";
		goals.add(g8);
		String g9 = "[](<>(((rc)&&(gc))||((!rc)&&(!gc))))";
		goals.add(g9);
		String g10 = "[](<>(((rd)&&(gd))||((!rd)&&(!gd))))";
		goals.add(g10);
		String g11 = "[](<>(((re)&&(ge))||((!re)&&(!ge))))";
		goals.add(g11);
		String g12 = "[](<>(((rf)&&(gf))||((!rf)&&(!gf))))";
		goals.add(g12);

//		
//		System.out.println("DOM: "+dom);
//		System.out.println("GOALS: "+goals);
		
		String cmd = "";
		for (String s: dom)
			cmd += "'-d="+s+"' ";
		for (String g: goals)
			cmd += "'-g="+g+"' ";
		System.out.println(cmd);
		
		BCLearner learner = new BCLearner(dom, goals);
		learner.learnBC();
		
	}

	@Test
	public void tesAMBA_02() throws ParseErrorException, InvalidConfigurationException {
		//Counter-Strategy Guided Refinement of GR(1) Temporal Logic Specifications
		//ARM’s Advanced Microcontroller Bus Architecture (AMBA) defines the Advanced High-Performance Bus (AHB)
		String d1 = "!hready";
		String d2 = "!hbusreq0";
		String d3 = "!hlock0";
		String d4 = "!hbusreq1";
		String d5 = "!hlock1";
		String d6 = "!hburst0";
		String d7 = "!hburst1";
		String d8 = "[](<>(!stateA1))";
		String d9 = "[](<>(hready))";
		
		String g0 = "!hmaster0";
		String g1 = "!hmastlock";
		String g2 = "start";
		String g3 = "decide";
		String g4 = "!locked";
		String g5 = "hgrant0";
		String g6 = "!hgrant1";
		String g7 = "!busreq";
		String g8 = "!stateA1";
		String g9 = "!stateG2";
		String g10 = "!stateG2_0";
		String g11 = "!stateG2_1";
		String g12 = "!stateG3_0";
		String g13 = "!stateG3_1";
		String g14 = "!stateG3_2";
		String g15 = "!stateG10_1";
		String g16 = "[](hlock0 -> hbusreq0)";
		String g17 = "[](hlock1 -> hbusreq1)";
		String g18 = "[]((!hmaster0) -> (!hbusreq0 <->!busreq))";
		String g19 = "[]((hmaster0) -> (!hbusreq1 <-> !busreq))";
		String g20 = "[](( (!stateA1) && ((!hmastlock)||(hburst0)||(hburst1))) -> (X(!stateA1)))";
		String g21 = "[](((!stateA1) && (hmastlock) && (!hburst0) && (!hburst1)) -> (X(stateA1)))";
		String g22 = " [](((stateA1) && (busreq)) -> (X(stateA1)))";
		String g23 = "[](((stateA1) && (!busreq)) ->(X(!stateA1)))";
		String g24 = "[]((!hready) -> (X(!start)))";
		String g25 = "[](((!stateG2) && ((!hmastlock)||(!start)||(hburst0)||(hburst1))) -> (X(!stateG2)))";
		String g26 = "[](((!stateG2) && (hmastlock) && (start) && (!hburst0) && (!hburst1)) -> (X(stateG2)))";
		String g27 = "[](((stateG2) && (!start) && (busreq)) -> (X(stateG2)))";
		String g28 = "[](!((stateG2) && (start)))";
		String g29 = "[](((stateG2) && (!start) && (!busreq)) -> (X(!stateG2)))";
		
//		String g30 = "[](((!stateG3_0) && (!stateG3_1) && (!stateG3_2) && ((!hmastlock) || (!start) || ((hburst0)||(!hburst1)))) -> ((X(!stateG3_0)) && (X(!stateG3_1)) && (X(!stateG3_2))))";
		String g30a = "[](((!stateG3_0) && (!stateG3_1) && (!stateG3_2) && ((!hmastlock) || (!start) || ((hburst0)||(!hburst1)))) -> X(!stateG3_0))";
		String g30b = "[](((!stateG3_0) && (!stateG3_1) && (!stateG3_2) && ((!hmastlock) || (!start) || ((hburst0)||(!hburst1)))) -> X(!stateG3_1))";
		String g30c = "[](((!stateG3_0) && (!stateG3_1) && (!stateG3_2) && ((!hmastlock) || (!start) || ((hburst0)||(!hburst1)))) -> X(!stateG3_2))";
		
//		String g31 = "[](((!stateG3_0) && (!stateG3_1) && (!stateG3_2) && ((hmastlock) && (start) && ((!hburst0) && (hburst1)) && (!hready))) -> ((X(stateG3_0)) && (X(!stateG3_1)) && (X(!stateG3_2))))";
		String g31a = "[](((!stateG3_0) && (!stateG3_1) && (!stateG3_2) && ((hmastlock) && (start) && ((!hburst0) && (hburst1)) && (!hready))) -> X(stateG3_0))";
		String g31b = "[](((!stateG3_0) && (!stateG3_1) && (!stateG3_2) && ((hmastlock) && (start) && ((!hburst0) && (hburst1)) && (!hready))) -> X(!stateG3_1))";
		String g31c = "[](((!stateG3_0) && (!stateG3_1) && (!stateG3_2) && ((hmastlock) && (start) && ((!hburst0) && (hburst1)) && (!hready))) -> X(!stateG3_2))";
		
//		String g32 = "[](((!stateG3_0) && (!stateG3_1) && (!stateG3_2) && ((hmastlock) && (start) && ((!hburst0) && (hburst1)) && (hready))) -> ((X(!stateG3_0)) && (X(stateG3_1)) && (X(!stateG3_2))))";
		String g32a = "[](((!stateG3_0) && (!stateG3_1) && (!stateG3_2) && ((hmastlock) && (start) && ((!hburst0) && (hburst1)) && (hready))) -> X(!stateG3_0))";
		String g32b = "[](((!stateG3_0) && (!stateG3_1) && (!stateG3_2) && ((hmastlock) && (start) && ((!hburst0) && (hburst1)) && (hready))) -> X(stateG3_1))";
		String g32c = "[](((!stateG3_0) && (!stateG3_1) && (!stateG3_2) && ((hmastlock) && (start) && ((!hburst0) && (hburst1)) && (hready))) -> X(!stateG3_2))";
		
		String g33 = "[](((stateG3_0) && (!stateG3_1) && (!stateG3_2) && ((!start) && (!hready))) -> ((X(stateG3_0)) && (X(!stateG3_1)) && (X(!stateG3_2))))";
		String g34 = "[](((stateG3_0) && (!stateG3_1) && (!stateG3_2) && ((!start) && (hready))) -> ((X(!stateG3_0)) && (X(stateG3_1)) && (X(!stateG3_2))))";
		String g35 = "[](!((stateG3_0) && (!stateG3_1) && (!stateG3_2) && ((start))))";
		String g36 = "[](((!stateG3_0) && (stateG3_1) && (!stateG3_2) && ((!start) && (!hready))) -> ((X(!stateG3_0)) && (X(stateG3_1)) && (X(!stateG3_2))))";
		String g37 = "[](((!stateG3_0) && (stateG3_1) && (!stateG3_2) && ((!start) && (hready))) -> ((X(stateG3_0)) && (X(stateG3_1)) && (X(!stateG3_2))))";
		String g38 = "[](!((!stateG3_0) && (stateG3_1) && (!stateG3_2) && ((start))))";
		String g39 = "[](((stateG3_0) && (stateG3_1) && (!stateG3_2) && ((!start) && (!hready))) -> ((X(stateG3_0)) && (X(stateG3_1)) && (X(!stateG3_2))))";
		String g40 = "[](((stateG3_0) && (stateG3_1) && (!stateG3_2) && ((!start) && (hready))) -> ((X(!stateG3_0)) && (X(!stateG3_1)) && (X(stateG3_2))))";
		String g41 = "[](!((stateG3_0) && (stateG3_1) && (!stateG3_2) && ((start))))";
		String g42 = "[](((!stateG3_0) && (!stateG3_1) && (stateG3_2) && ((!start) && (!hready))) -> ((X(!stateG3_0)) && (X(!stateG3_1)) && (X(stateG3_2))))";
		String g43 = "[](((!stateG3_0) && (!stateG3_1) && (stateG3_2) && ((!start) && (hready))) -> ((X(!stateG3_0)) && (X(!stateG3_1)) && (X(!stateG3_2))))";
		String g44 = "[](!((!stateG3_0) && (!stateG3_1) && (stateG3_2) && ((start))))";
		String g45 = "[]((hready) -> ((hgrant0) && ((X(!hmaster0)))))";
		String g46 = "[]((hready) -> ((hgrant1) && ((X(hmaster0)))))";
		String g47 = "[]((hready) -> (!locked && (X(!hmastlock))))";
		String g48 = "[]((X(!start)) -> (((!hmaster0)) <-> ((X(!hmaster0)))))";
		String g49 = "[]((X(!start)) -> (((hmaster0)) <-> ((X(hmaster0)))))";
		String g50 = "[]((((X(!start)))) -> ((hmastlock) <-> (X(hmastlock))))";
		String g51 = "[]((decide && hlock0 && (X(hgrant0))) -> (X(locked)))";
		String g52 = "[]((decide && !hlock0 && (X(hgrant0))) -> (X(!locked)))";
		String g53 = "[]((decide && hlock1 && (X(hgrant1))) -> (X(locked)))";
		String g54 = "[]((decide && !hlock1 && (X(hgrant1))) -> (X(!locked)))";
		String g55 = "[]((!decide) -> (((!hgrant0) <-> (X(!hgrant0)))))";
		String g56 = "[]((!decide) -> (((!hgrant1) <-> (X(!hgrant1)))))";
		String g57 = "[]((!decide) -> (!locked <-> (X(!locked))))";
		String g58 = "[](((!stateG10_1) && (((hgrant1) || (hbusreq1)))) -> (X(!stateG10_1)))";
		String g59 = "[](((!stateG10_1) && ((!hgrant1) && (!hbusreq1))) -> (X(stateG10_1)))";
		String g60 = "[](((stateG10_1) && ((!hgrant1) && (!hbusreq1))) -> (X(stateG10_1)))";
		String g61 = "[](!((stateG10_1) && (((hgrant1)) && (!hbusreq1))))";
		String g62 = "[](((stateG10_1) && (hbusreq1)) -> (X(!stateG10_1)))";
		String g63 = "[]((decide && !hbusreq0 && !hbusreq1) -> (X(hgrant0)))";
		String g64 = "[](<>(!stateG2))";
		String g65 = "[](<>((!stateG3_0) && (!stateG3_1) && (!stateG3_2)))";
		String g66 = "[](<>(((!hmaster0)) || !hbusreq0))";
		String g67 = "[](<>(((hmaster0)) || !hbusreq1))";
		Set<String> dom = new HashSet<>();
		Set<String> goals = new HashSet<>();
		dom.add(d1);
		dom.add(d2);
		dom.add(d3);
		dom.add(d4);
		dom.add(d5);
		dom.add(d6);
		dom.add(d7);
		dom.add(d8);
		dom.add(d9);
		
		goals.add(g0);
		goals.add(g1);
		goals.add(g2);
		goals.add(g3);
		goals.add(g4);
		goals.add(g5);
		goals.add(g6);
		goals.add(g7);
		goals.add(g8);
		goals.add(g9);
		goals.add(g10);
		goals.add(g11);
		goals.add(g12);
		goals.add(g13);
		goals.add(g14);
		goals.add(g15);
		goals.add(g16);
		goals.add(g17);
		goals.add(g18);
		goals.add(g19);
		goals.add(g20);
		goals.add(g21);
		goals.add(g22);
		goals.add(g23);
		goals.add(g24);
		goals.add(g25);
		goals.add(g26);
		goals.add(g27);
		goals.add(g28);
		goals.add(g29);
		
//		//goals.add(g30);
		goals.add(g30a);
		goals.add(g30b);
		goals.add(g30c);
		
		//goals.add(g31);
		goals.add(g31a);
		goals.add(g31b);
		goals.add(g31c);
		
		
//		//goals.add(g32);
		goals.add(g32a);
		goals.add(g32b);
		goals.add(g32c);
		
		goals.add(g33);
		goals.add(g34);
		goals.add(g35);
		goals.add(g36);
		goals.add(g37);
		goals.add(g38);
		goals.add(g39);
		goals.add(g40);
		goals.add(g41);
		goals.add(g42);
		goals.add(g43);
		goals.add(g44);
		goals.add(g45);
		goals.add(g46);
		goals.add(g47);
		goals.add(g48);
		goals.add(g49);
		goals.add(g50);
		goals.add(g51);
		goals.add(g52);
		goals.add(g53);
		goals.add(g54);
		goals.add(g55);
		goals.add(g56);
		goals.add(g57);
		goals.add(g58);
		goals.add(g59);
		goals.add(g60);
		goals.add(g61);
		goals.add(g62);
		goals.add(g63);
		goals.add(g64);
		goals.add(g65);
		goals.add(g66);
		goals.add(g67);
		
		System.out.println("DOM: "+dom);
		System.out.println("GOALS: "+goals);
		
		BCLearner learner = new BCLearner(dom, goals);
		learner.learnBC();
	}
	
	
	
}
