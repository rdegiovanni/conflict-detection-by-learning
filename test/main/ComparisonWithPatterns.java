package main;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import dCNF.LTLSolver.SolverResult;
import geneticalgorithm.fitnessfunction.DCNFEvaluator;
import gov.nasa.ltl.trans.Formula;
import gov.nasa.ltl.trans.ParseErrorException;
import gov.nasa.ltl.trans.Parser;

public class ComparisonWithPatterns {

	@Test
	public void testAchievePattern() throws IOException, ParseErrorException {
		List<Solution> allSolutions = new LinkedList<>();
		for (int i=0; i<10;i++){
			String file = "case-studies/achievepattern/achievepattern-exec"+i+".txt";
			String [] arr = Statistics.loadFile(file,50);
			allSolutions.addAll(Statistics.getSolutions(arr));
		}
		
		Set<Formula<String>> tableauxBCs = new HashSet<>();
		tableauxBCs.add(Parser.parse("<> (p && r)"));
		
		computeRelation(allSolutions, tableauxBCs);
	}
	
	@Test
	public void testRetraction1Pattern() throws IOException, ParseErrorException {
		List<Solution> allSolutions = new LinkedList<>();
		for (int i=0; i<10;i++){
			String file = "case-studies/retractionpattern/retractionpattern1-exec"+i+".txt";
			String [] arr = Statistics.loadFile(file,50);
			allSolutions.addAll(Statistics.getSolutions(arr));
		}
		
		Set<Formula<String>> tableauxBCs = new HashSet<>();
		tableauxBCs.add(Parser.parse("<> (p && ((! q) U ([](!p))))"));
		
		computeRelation(allSolutions, tableauxBCs);
	}
	
	@Test
	public void testRetraction2Pattern() throws IOException, ParseErrorException {
		List<Solution> allSolutions = new LinkedList<>();
		for (int i=0; i<10;i++){
			String file = "case-studies/retractionpattern/retractionpattern2-exec"+i+".txt";
			String [] arr = Statistics.loadFile(file,50);
			allSolutions.addAll(Statistics.getSolutions(arr));
		}
		
		Set<Formula<String>> tableauxBCs = new HashSet<>();
		tableauxBCs.add(Parser.parse("<>((p && r && !s)U (p && !r && !s))"));
		
		computeRelation(allSolutions, tableauxBCs);
	}
	
	public static void computeRelation(List<Solution> allSolutions,Set<Formula<String>> tableauxBCs){
		Set<String> genetic_implies = new HashSet<>();
		Set<String> implied_by_tableaux = new HashSet<>();
		Set<String> equivalent = new HashSet<>();
		Set<String> nocomparable = new HashSet<>();

		for (Formula<String> bc : tableauxBCs){
			boolean relationFound = false;
			for (Solution s : allSolutions){
				Set<Formula<String>> f = new HashSet<>();
				boolean implies = false;
				//check s -> BC1
				f.add(s.BC);
				f.add(Formula.Not(bc));
				SolverResult sat = SolverResult.UNSAT;
				try{ sat = DCNFEvaluator.checkSAT(f); }
				catch (Exception e) {e.printStackTrace();}
				if (sat==SolverResult.UNSAT) {
					implies = true;
				} 
				
				//check BC -> s
				f.clear();
				f.add(bc);
				f.add(Formula.Not(s.BC));
				sat = SolverResult.UNSAT;
				try{ sat = DCNFEvaluator.checkSAT(f); }
				catch (Exception e) {e.printStackTrace();}
				
				if (sat==SolverResult.UNSAT && implies) {
					equivalent.add(bc.toString());
					relationFound = true;
					
				} 
				else if (sat==SolverResult.UNSAT && !implies) {
					implied_by_tableaux.add(bc.toString());
					relationFound = true;
					
				} 
				else if (sat!=SolverResult.UNSAT && implies) {
					genetic_implies.add(bc.toString());
					relationFound = true;					
				}
			}
			if(!relationFound){
				nocomparable.add(bc.toString());
			}
		}
		System.out.println();
		if (!equivalent.isEmpty())
			System.out.println("Equivalent ("+equivalent.size()+"): "+equivalent);
		if (!genetic_implies.isEmpty())
			System.out.println("Our BCs imply tableaux ("+genetic_implies.size()+"): " + genetic_implies);
		if (!implied_by_tableaux.isEmpty())
			System.out.println("Tableaux implies our BCs ("+implied_by_tableaux.size()+"): "+implied_by_tableaux);
		if (!nocomparable.isEmpty())
			System.out.println("No Comparables ("+nocomparable.size()+"): "+nocomparable);
	}

}
