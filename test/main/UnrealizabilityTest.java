package main;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.jgap.InvalidConfigurationException;
import org.junit.Test;

import gov.nasa.ltl.trans.ParseErrorException;

public class UnrealizabilityTest {

	@Test
	public void tesLiftController() throws ParseErrorException, InvalidConfigurationException {
		//Counter-Strategy Guided Refinement of GR(1) Temporal Logic Specifications
		String d1 = "!b1 && !b2 && !b3";
		String d2 = "[]((b1 && f1) -> X(!b1))";
		String d3 = "[]((b2 && f2) -> X(!b2))";
		String d4 = "[]((b3 && f3) -> X(!b3))";
		String d5 = "[]((b1 && !f1) -> X(b1))";
		String d6 = "[]((b2 && !f2) -> X(b2))";
		String d7 = "[]((b3 && !f3) -> X(b3))";
		String g1 = "f1 && !f2 && !f3";
		String g2 = "[](!(f1 && f2) && !(f1 && f3) && !(f2 && f3))";
		String g3 = "[](f1 -> X (f1 || f2))";
		String g4 = "[](f2 -> X (f1 || f2 || f3))";
		String g5 = "[](f3 -> X (f2 || f3))";
		String g6 = "[](((f1 && X(f2)) || (f2 && X(f3))) -> X (b1 || b2 || b3))";
		String g7 = "[]<>(b1 -> f1)";
		String g8 = "[]<>(b2 -> f2)";
		String g9 = "[]<>(b3 -> f3)";
		String g10 = "[]<>(f1)";
		String g11 = "[]<>(f2)";
		String g12 = "[]<>(f3)";
		Set<String> dom = new HashSet<>();
		dom.add(d1);
		dom.add(d2);
		dom.add(d3);
		dom.add(d4);
		dom.add(d5);
		dom.add(d6);
		dom.add(d7);
		Set<String> goals = new HashSet<>();
		goals.add(g1);
		goals.add(g2);
		goals.add(g3);
		goals.add(g4);
		goals.add(g5);
		goals.add(g6);
		goals.add(g7);
		goals.add(g8);
		goals.add(g9);
		goals.add(g10);
		goals.add(g11);
		goals.add(g12);
		System.out.println("DOM: "+dom);
		System.out.println("GOALS: "+goals);
		
		BCLearner learner = new BCLearner(dom, goals);
		learner.learnBC();
	}
	
	@Test
	public void tesLiftControllerTwoFLoors() throws ParseErrorException, InvalidConfigurationException {
		//Counter-Strategy Guided Refinement of GR(1) Temporal Logic Specifications
		String d1 = "!b1 && !b2";
		String d2 = "[]((b1 && f1) -> X(!b1))";
		String d3 = "[]((b2 && f2) -> X(!b2))";
		String d5 = "[]((b1 && !f1) -> X(b1))";
		String d6 = "[]((b2 && !f2) -> X(b2))";
		String g1 = "f1 && !f2";
		String g2 = "[](!(f1 && f2))";
		String g3 = "[](f1 -> X (f1 || f2))";
		String g4 = "[](f2 -> X (f1 || f2))";
		String g6 = "[]((f1 && X(f2)) -> X (b1 || b2))";
		String g7 = "[]<>(b1 -> f1)";
		String g8 = "[]<>(b2 -> f2)";
		String g10 = "[]<>(f1)";
		String g11 = "[]<>(f2)";
	
		Set<String> dom = new HashSet<>();
		dom.add(d1);
		dom.add(d2);
		dom.add(d3);
		dom.add(d5);
		dom.add(d6);
		Set<String> goals = new HashSet<>();
		goals.add(g1);
		goals.add(g2);
		goals.add(g3);
		goals.add(g4);
		goals.add(g6);
		goals.add(g7);
		goals.add(g8);
		goals.add(g10);
		goals.add(g11);
		System.out.println("DOM: "+dom);
		System.out.println("GOALS: "+goals);
		
		BCLearner learner = new BCLearner(dom, goals);
		learner.learnBC();
	}
	
	
	@Test
	public void testUnrealizability0() throws ParseErrorException, InvalidConfigurationException {
		//Counter-Strategy Guided Refinement of GR(1) Temporal Logic Specifications
		String g1 = "[](r -> X(<>(g)))";
		String g2 = "[]((c || g) -> X(!g))";
		String g3 = "[](c -> !v)";
		String g4 = "[]<>(g && v)";
		Set<String> dom = new HashSet<>();
		Set<String> goals = new HashSet<>();
		goals.add(g1);
		goals.add(g2);
		goals.add(g3);
		goals.add(g4);
		System.out.println("DOM: "+dom);
		System.out.println("GOALS: "+goals);
		
		BCLearner learner = new BCLearner(dom, goals);
		learner.learnBC();
	}
	
	@Test
	public void tesUnsynthSimple() throws ParseErrorException, InvalidConfigurationException {
		//Analyzing Unsynthesizable Specifications for High-Level Robot Behavior Using LTLMoP
		String d1 = "!fire";
		String d2 = "!hazardous_item";
		String d3 = "[]<>(true)";
		
		String g1 = "(!bit0 && bit1 && !bit2)";
		String g2 = "[]( !X(bit0) && !X(bit1) && !X(bit2) )";
		String g3 = "[]<>(true)";
		String g4 = "( (!bit0 && !bit1 && !bit2) || (!bit0 && !bit1 && bit2)"
				 +"|| (!bit0 && bit1 && !bit2) || (!bit0 && bit1 && bit2)"
				 +"|| (bit0 && !bit1 && !bit2) || (bit0 && !bit1 && bit2) ) ";
		String g5 = "[]( ((!bit0 && !bit1 && !bit2)) -> ( ((!X(bit0) && !X(bit1) && !X(bit2)))"
									+"|| ((!X(bit0) && X(bit1) && X(bit2)))" 
									+"|| ((X(bit0) && !X(bit1) && !X(bit2)))  ) )";
		String g6 = "[]( ((!bit0 && !bit1 && bit2)) -> ( ((!X(bit0) && !X(bit1) && X(bit2)))"
									+"|| ((!X(bit0) && X(bit1) && X(bit2)))"
									+"|| ((X(bit0) && !X(bit1) && !X(bit2)))  ) )";
		String g7 = "[]( ((!bit0 && bit1 && !bit2)) -> ( ((!X(bit0) && X(bit1) && !X(bit2)))"
									+"|| ((X(bit0) && !X(bit1) && !X(bit2)))"
									+"|| ((X(bit0) && !X(bit1) && X(bit2)))  ) )";
		String g8 = "[]( ((!bit0 && bit1 && bit2)) -> ( ((!X(bit0) && X(bit1) && X(bit2)))"
									+"|| ((!X(bit0) && !X(bit1) && !X(bit2))) "
									+"|| ((!X(bit0) && !X(bit1) && X(bit2)))" 
									+"|| ((X(bit0) && !X(bit1) && X(bit2)))  ) )";
		String g9 = "[]( ((bit0 && !bit1 && !bit2)) -> ( ((X(bit0) && !X(bit1) && !X(bit2)))"
									+"|| ((!X(bit0) && !X(bit1) && !X(bit2))) "
									+"|| ((!X(bit0) && !X(bit1) && X(bit2))) "
									+"|| ((!X(bit0) && X(bit1) && !X(bit2)))  ) )";
		String g10 = "[]( ((bit0 && !bit1 && bit2)) -> ( ((X(bit0) && !X(bit1) && X(bit2)))"
									+"|| ((!X(bit0) && X(bit1) && !X(bit2))) "
									+"|| ((!X(bit0) && X(bit1) && X(bit2)))  ) )";
		String g11 = "[]( (!bit0 && !bit1 && !bit2) || (!bit0 && !bit1 && bit2)"
				+ "|| (!bit0 && bit1 && !bit2) || (!bit0 && bit1 && bit2)"
				+ "|| (bit0 && !bit1 && !bit2) || (bit0 && !bit1 && bit2)) ";
		Set<String> dom = new HashSet<>();
		dom.add(d1);
		dom.add(d2);
		dom.add(d3);
		Set<String> goals = new HashSet<>();
		goals.add(g1);
		goals.add(g2);
		goals.add(g3);
		goals.add(g4);
		goals.add(g5);
		goals.add(g6);
		goals.add(g7);
		goals.add(g8);
		goals.add(g9);
		goals.add(g10);
		goals.add(g11);
		System.out.println("DOM: "+dom);
		System.out.println("GOALS: "+goals);
		
		String cmd = "";
		for (String s: dom)
			cmd += "'-d="+s+"' ";
		for (String g: goals)
			cmd += "'-g="+g+"' ";
		System.out.println(cmd);
//		BCLearner learner = new BCLearner(dom, goals);
//		learner.learnBC();
	}


	@Test
	public void tesUnsynthCounter() throws ParseErrorException, InvalidConfigurationException {
		//Analyzing Unsynthesizable Specifications for High-Level Robot Behavior Using LTLMoP
		String d1 = "!fire";
		String d2 = "!person";
		String d3 = "[](!(X(fire) && X(person)))";
		String d4 = "[]<>(true)";
		
		String g0 = "!radio";
		String g1 = "(!bit0 && bit1 && !bit2)";
		String g2 = "[]((( X(fire) ) ) -> ( ! ((!X(bit0) && X(bit1) && X(bit2)))) )";
		String g3 = "[]((( X(pern) ) ) -> (   !  ((X(bit0) && !X(bit1) && !X(bit2)))) )";
		String g4 = "[](!X(radio))";
		String g5 = "[]( ((X(bit0) && !X(bit1) && !X(bit2)))  ||  ((!X(bit0) && !X(bit1) && !X(bit2)))  ||  ((!X(bit0) && X(bit1) && !X(bit2)))  ||  ((X(bit0) && !X(bit1) && X(bit2)))  ||  ((!X(bit0) && !X(bit1) && X(bit2)))  ||  ((!X(bit0) && X(bit1) && X(bit2))))";
		String g6 = "[]<>( ((!bit0 && !bit1 && !bit2)))";
		String g7 = "( (!bit0 && !bit1 && !bit2) "
				+ "|| (!bit0 && !bit1 && bit2)"
				+ "|| (!bit0 && bit1 && !bit2)"
				+ "|| (!bit0 && bit1 && bit2)"
				+ "|| (bit0 && !bit1 && !bit2)"
				+ "|| (bit0 && !bit1 && bit2)) ";
		String g8 = "[]( ((!bit0 && !bit1 && !bit2)) -> ( ((!X(bit0) && !X(bit1) && !X(bit2)))"
				+ "|| ((!X(bit0) && X(bit1) && X(bit2))) "
				+ "|| ((X(bit0) && !X(bit1) && !X(bit2)))  ) )";
		String g9 = "[]( ((!bit0 && !bit1 && bit2)) -> ( ((!X(bit0) && !X(bit1) && X(bit2)))"
				+ "|| ((!X(bit0) && X(bit1) && X(bit2))) "
				+ "|| ((X(bit0) && !X(bit1) && !X(bit2)))  ) ) ";
		String g10 = "[]( ((!bit0 && bit1 && !bit2)) -> ( ((!X(bit0) && X(bit1) && !X(bit2)))"
				+ "|| ((X(bit0) && !X(bit1) && !X(bit2)))"
				+ "|| ((X(bit0) && !X(bit1) && X(bit2)))  ) ) ";
		String g11 = "[]( ((!bit0 && bit1 && bit2)) -> ( ((!X(bit0) && X(bit1) && X(bit2)))"
				+ "|| ((!X(bit0) && !X(bit1) && !X(bit2)))"
				+ "|| ((!X(bit0) && !X(bit1) && X(bit2)))"
				+ "|| ((X(bit0) && !X(bit1) && X(bit2)))  ) )";
		String g12 = "[]( ((bit0 && !bit1 && !bit2)) -> ( ((X(bit0) && !X(bit1) && !X(bit2)))"
				+ "|| ((!X(bit0) && !X(bit1) && !X(bit2)))"
				+ "|| ((!X(bit0) && !X(bit1) && X(bit2)))"
				+ "|| ((!X(bit0) && X(bit1) && !X(bit2)))  ) )";
		String g13 = "[]( ((bit0 && !bit1 && bit2)) -> ( ((X(bit0) && !X(bit1) && X(bit2)))"
				+ "|| ((!X(bit0) && X(bit1) && !X(bit2)))"
				+ "|| ((!X(bit0) && X(bit1) && X(bit2)))  ) )";
		String g14 = "( (!bit0 && !bit1 && !bit2) "
				+ "|| (!bit0 && !bit1 && bit2)"
				+ "|| (!bit0 && bit1 && !bit2)"
				+ "|| (!bit0 && bit1 && bit2)"
				+ "|| (bit0 && !bit1 && !bit2)"
				+ "|| (bit0 && !bit1 && bit2)) ";
		
		Set<String> dom = new HashSet<>();
		dom.add(d1);
		dom.add(d2);
		dom.add(d3);
		dom.add(d4);
		
		Set<String> goals = new HashSet<>();
		goals.add(g0);
		goals.add(g1);
		goals.add(g2);
		goals.add(g3);
		goals.add(g4);
		goals.add(g5);
		goals.add(g6);
		goals.add(g7);
		goals.add(g8);
		goals.add(g9);
		goals.add(g10);
		goals.add(g11);
		goals.add(g12);
		goals.add(g13);
		goals.add(g14);
		System.out.println("DOM: "+dom);
		System.out.println("GOALS: "+goals);
		
		String cmd = "";
		for (String s: dom)
			cmd += "'-d="+s+"' ";
		for (String g: goals)
			cmd += "'-g="+g+"' ";
		System.out.println(cmd);
		BCLearner learner = new BCLearner(dom, goals);
		learner.learnBC();
	}


}
