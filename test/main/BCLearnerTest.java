package main;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.jgap.InvalidConfigurationException;
import org.junit.Test;

import gov.nasa.ltl.trans.Formula;
import gov.nasa.ltl.trans.ParseErrorException;
import gov.nasa.ltl.trans.Parser;

public class BCLearnerTest {


	@Test
	public void testTableauExample() throws ParseErrorException, InvalidConfigurationException {
		String g1 = "[](X(p))";
		String g2 = "<>(! p)";
		Set<String> dom = new HashSet<>();
		Set<String> goals = new HashSet<>();
		goals.add(g1);
		goals.add(g2);
		System.out.println("DOM: "+dom);
		System.out.println("GOALS: "+goals);
		
		BCLearner learner = new BCLearner(dom, goals);
		learner.learnBC();
		
	}
	
	@Test
	public void testMinePump() throws ParseErrorException, InvalidConfigurationException {
		String d = "[]((p && X(p)) -> X(X(! h)))";
		String g1 = "[](h -> X(p))";
		String g2 = "[](m -> X(! p))";
		Set<String> dom = new HashSet<>();
		dom.add(d);
		Set<String> goals = new HashSet<>();
		goals.add(g1);
		goals.add(g2);
		System.out.println("DOM: "+dom);
		System.out.println("GOALS: "+goals);
		
		BCLearner learner = new BCLearner(dom, goals);
		learner.learnBC();
		
	}
	
	@Test
	public void testATM() throws ParseErrorException, InvalidConfigurationException {
		String d = "[](l -> <>(! l))";
		String g1 = "[]((p && ! l) -> m)";
		String g2 = "[]((! p) -> ((! m) && X(l)))";
		Set<String> dom = new HashSet<>();
		dom.add(d);
		Set<String> goals = new HashSet<>();
		goals.add(g1);
		goals.add(g2);
		System.out.println("DOM: "+dom);
		System.out.println("GOALS: "+goals);
		
		BCLearner learner = new BCLearner(dom, goals);
		learner.learnBC();
	}
	
	@Test
	public void testElevator() throws ParseErrorException, InvalidConfigurationException {
		String d = "[] ( X(open) -> atfloor)";
		String g1 = "[](call -> <>(open))";
		Set<String> dom = new HashSet<>();
		dom.add(d);
		Set<String> goals = new HashSet<>();
		goals.add(g1);
		System.out.println("DOM: "+dom);
		System.out.println("GOALS: "+goals);
		
		BCLearner learner = new BCLearner(dom, goals);
		learner.learnBC();
	}
	

	
	@Test
	public void testRRCS() throws ParseErrorException, InvalidConfigurationException {
		String d1 = "[] (ta <-> X(tc))";
		String d2 = "[] (X(cc) -> ca && go)";
		String g1 = "[] (tc -> !cc)";
		String g2 = "[] (ta -> !go)";
		Set<String> dom = new HashSet<>();
		dom.add(d1);
		dom.add(d2);
		dom.add("!ta && !ca");
		Set<String> goals = new HashSet<>();
		goals.add(g1);
		goals.add(g2);
		System.out.println("DOM: "+dom);
		System.out.println("GOALS: "+goals);
		
		BCLearner learner = new BCLearner(dom, goals);
		learner.learnBC();
	}

	@Test
	public void testRRCS_spin2015() throws ParseErrorException, InvalidConfigurationException {
		Set<String> dom = new HashSet<>();
		Set<String> goals = new HashSet<>();

		
		String d1 = "[] (ta -> X(!ta && tc))";
		String d2 = "[] (tc -> X(!tc))";
		String d3 = "[] (ca && go -> X(cc && !ca))";
		String d4 = "[] (cc -> X(!cc))";
		String init_train = "!ta && !tc";
		String init_car = "!ca && !cc";
		String init_gate = "!go";
		
//		dom.add(init_train);
//		dom.add(init_car);
//		dom.add(init_gate);
		dom.add(d1);
		dom.add(d2);
		dom.add(d3);
		dom.add(d4);
		
		String g1 = "[] (ta -> X(!go)";
		String g2 = "[] (tc -> X(go))";
		String g3 = "[] (!(tc && cc))";		
		goals.add(g1);
		goals.add(g2);
		goals.add(g3);
		
		System.out.println("DOM: "+dom);
		System.out.println("GOALS: "+goals);
		
		BCLearner learner = new BCLearner(dom, goals);
		learner.learnBC();
	}
	@Test
	public void testTCP() throws ParseErrorException, InvalidConfigurationException {
		String g1 = "[](send -> (!ack U delivered))";
		String g2 = "[] (delivered -> (!send U ack))";
		Set<String> dom = new HashSet<>();
		Set<String> goals = new HashSet<>();
		goals.add(g1);
		goals.add(g2);
		System.out.println("DOM: "+dom);
		System.out.println("GOALS: "+goals);
		
		BCLearner learner = new BCLearner(dom, goals);
		learner.learnBC();
	}

	@Test
	public void testTelephone() throws ParseErrorException, InvalidConfigurationException {
		String d1 = "[](o -> ! f)";
		String d2 = "[](o -> ! c)";
		String d3 = "[](c -> ! f)";
		String g1 = "[](c  -> (c  U (o || d)) )";
		String g2 = "[](c  -> (c  U (f || d)) )";
		Set<String> dom = new HashSet<>();
		dom.add(d1);
		dom.add(d2);
		dom.add(d3);
		Set<String> goals = new HashSet<>();
		goals.add(g1);
		goals.add(g2);
		System.out.println("DOM: "+dom);
		System.out.println("GOALS: "+goals);
		
		BCLearner learner = new BCLearner(dom, goals);
		learner.learnBC();
	}
	
	@Test
	public void testLAS() throws ParseErrorException, InvalidConfigurationException {
		String g1 = "[](a -> m)";
		String g2 = "[]((c && m && n) -> d)";
		String g3 = "[]((c && (! n)) -> (! d))";
		String g4 = "[]((! h) -> (! n))";
		String g5 = "[]((! n) -> (! i))";
		Set<String> dom = new HashSet<>();
		Set<String> goals = new HashSet<>();
		goals.add(g1);
		goals.add(g2);
		goals.add(g3);
		goals.add(g4);
		goals.add(g5);
		
//		System.out.println("DOM: "+dom);
//		System.out.println("GOALS: "+goals);
		
		String cmd = "";
		for (String s: dom)
			cmd += "'-d="+s+"' ";
		for (String g: goals)
			cmd += "'-g="+g+"' ";
		System.out.println(cmd);
		
//		
//		BCLearner learner = new BCLearner(dom, goals);
//		learner.learnBC();
	}
	
	@Test
	public void testAchievePattern() throws ParseErrorException, InvalidConfigurationException {
		String d = "[] (q -> s)";
		String g1 = "[] (p -> <>(q))";
		String g2 = "[] (r -> [](!s))";
		Set<String> dom = new HashSet<>();
		dom.add(d);
		Set<String> goals = new HashSet<>();
		goals.add(g1);
		goals.add(g2);
		System.out.println("DOM: "+dom);
		System.out.println("GOALS: "+goals);
		
		BCLearner learner = new BCLearner(dom, goals);
		learner.learnBC();
	}
	
	@Test
	public void testRetractionPattern1() throws ParseErrorException, InvalidConfigurationException {
		String g1 = "[] (p -> <>(q))";
		String g2 = "[] (q -> p) ";
		Set<String> dom = new HashSet<>();
		Set<String> goals = new HashSet<>();
		goals.add(g1);
		goals.add(g2);
		System.out.println("DOM: "+dom);
		System.out.println("GOALS: "+goals);
		
		BCLearner learner = new BCLearner(dom, goals);
		learner.learnBC();
	}
	
	@Test
	public void testRetractionPattern2() throws ParseErrorException, InvalidConfigurationException {
		String g1 = "[] (p -> (q W s))";
		String g2 = "[] (q -> r)";
		Set<String> dom = new HashSet<>();
		Set<String> goals = new HashSet<>();
		goals.add(g1);
		goals.add(g2);
		System.out.println("DOM: "+dom);
		System.out.println("GOALS: "+goals);
		
		BCLearner learner = new BCLearner(dom, goals);
		learner.learnBC();
	}
	
	@Test
	public void testAeroplaneControlSystem() throws ParseErrorException, InvalidConfigurationException {
		String d1 = "[](a <-> b)";
		String d2 = "<>(l)";
		String d3 = "[](l -> <>(a))";
		String d4 = "[] (!l -> !b)";
		String d5 = "[](u -> <> (a))";
		String d6 = "[](u -> c)";
		String g1 = "[](l -> <>([](b) && <>([](c))))";
		String g2 = "[](l -> <>(b U c))";
		String g3 = "[](!b -> !u)";
		String g4 = "<>(l U (u U c))";
		//--Forbidden F(a && F(!a))
		String g5 = "[](a -> [](a))";
		Set<String> dom = new HashSet<>();
		dom.add(d1);
		dom.add(d2);
		dom.add(d3);
		dom.add(d4);
		dom.add(d5);
		dom.add(d6);
		Set<String> goals = new HashSet<>();
		goals.add(g1);
		goals.add(g2);
		goals.add(g3);
		goals.add(g4);
		goals.add(g5);
		System.out.println("DOM: "+dom);
		System.out.println("GOALS: "+goals);
		
		BCLearner learner = new BCLearner(dom, goals);
		learner.learnBC();
	}
	
	
	
}
