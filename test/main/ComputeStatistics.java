package main;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

import gov.nasa.ltl.trans.Formula;
import gov.nasa.ltl.trans.ParseErrorException;
import gov.nasa.ltl.trans.Parser;

public class ComputeStatistics {

	@Test
	public void test() throws IOException, ParseErrorException {
		String file = "case-studies/rrcs/rrcs-exec1.txt";
		
		String [] arr = loadFile(file,50);
		List<Solution> solutions = getSolutions(arr);
		System.out.println(arr[0]);
		Solution best = null;
		for (Solution s : solutions){
			if (best==null)
				best = s;
			else if (best.fitness<s.fitness)
				best = s;
		}
		Solution first = solutions.get(0);
		int totalTime = getTotalTime(arr[arr.length-1]);
		int numOfSol = getNumOfSol(arr[arr.length-1]);
		int nonEqSolutions = getEqSol(arr[arr.length-1]);
		System.out.println("Min. " + first.iteration + " - Accumulated Learning Time : " + first.time + " - Best. "+best.iteration+ " Accumulated Learning Time: "+best.time+" Total time: "+totalTime+ " Sols: "+numOfSol+" NonEq. " + nonEqSolutions);
	}
	
	@Test
	public void testAvg() throws IOException, ParseErrorException {
		for (int i=0; i<10;i++){
			String file = "case-studies/minepump/minepump-exec"+i+".txt";		
			String [] arr = loadFile(file,50);
			List<Solution> solutions = getSolutions(arr);
//			System.out.println(arr[0]);
			Solution best = null;
			for (Solution s : solutions){
				if (best==null)
					best = s;
				else if (best.fitness<s.fitness)
					best = s;
			}
			Solution first = solutions.get(0);
			int totalTime = getTotalTime(arr[arr.length-1]);
			int numOfSol = getNumOfSol(arr[arr.length-1]);
			int nonEqSolutions = getEqSol(arr[arr.length-1]);
			System.out.println("Min. " + first.iteration + " - Accumulated Learning Time : " + first.time + " - Best. "+best.iteration+ " Accumulated Learning Time: "+best.time+" Total time: "+totalTime+ " Sols: "+numOfSol+" NonEq. " + nonEqSolutions);
		}
	}

	@Test
	public void testRQ2() throws IOException, ParseErrorException {
		int min = 0;
		int minT = 0;
		int max = 0;
		int maxT = 0;
		int avg = 0;
		int avgT = 0;
		int numOfSol = 0;
		for (int i=0; i<10;i++){
			String file = "case-studies/tcp/tcp-exec"+i+".txt";		
			String [] arr = loadFile(file,50);
			
			try{
			int totalTime = getTotalTime(arr[arr.length-1]);
			int nonEqSolutions = getEqSol(arr[arr.length-1]);
			
			if (nonEqSolutions==0)
				continue;
			
			if (min==0){
				min = nonEqSolutions;
				minT = totalTime;
			}
			
			if (nonEqSolutions<min){
				min = nonEqSolutions;
				minT = totalTime;
			}
			
			if (nonEqSolutions>max){
				max = nonEqSolutions;
				maxT = totalTime;
			}

			avg += nonEqSolutions;
			avgT += totalTime;
			numOfSol++;
			
			System.out.println(" Total time: "+totalTime+ " NonEq. " + nonEqSolutions);
			}
			catch(Exception e){
				continue;
			}
		}
		System.out.println(numOfSol);
		System.out.println(min + "\t" + minT + "\t" + max + "\t" + maxT + "\t" + (avg/numOfSol) + "\t" + (avgT/numOfSol));
	}
	
	public static String[] loadFile(String filename, int iterations) throws IOException{
		BufferedReader br = new BufferedReader(new FileReader(filename));
		String[] arr = new String[iterations];
		try {
		    int i = -1;
		    String line = br.readLine();
		    while (line != null) {
		    	if (line.startsWith("GENERATION")){
		    		i++;
		    		arr[i] = line;
		    	}
		    	else{
		    		if (i>=0)
		    			arr[i] += "\n"+line;
		    	}
		    	
		    	line = br.readLine();
		    }
		}
		finally {
	    br.close();
		}
		return arr;
	}
	
	public static List<Solution> getSolutions(String [] arr) throws ParseErrorException{
		List<Solution> solutions = new LinkedList<>();
	    
	    for (int i=0 ; i<arr.length;i++){
	    	if (arr[i].contains("Fittest")){
	    		String [] gen = arr[i].split("Accumulated Learning Time :");
	    		int time = Integer.valueOf(gen[1].split("\n")[0]);
	    		gen = arr[i].split("Fittest value: ");
	    		double fitness = Double.valueOf(gen[1].split(" ")[0]);
	    		gen = arr[i].split("for chromosome ");
	    		Formula<String> f = Parser.parse(gen[1].split("\n")[0]);
	    		Solution s = new Solution(f, fitness, time, i+1);
	    		solutions.add(s);
	    	}
	    }
	    return solutions;
	}
	
	public static int getTotalTime (String line){
		if (line.contains("TOTAL Learing Time :"))
			return Integer.valueOf(line.split("TOTAL Learing Time :")[1].split("\n")[0]);
		return 0;
	}
	
	public static int getNumOfSol (String line){
		if (line.contains("Solutions: "))
			return Integer.valueOf(line.split("Solutions: ")[1].split("\n")[0]);
		return 0;
	}
	
	public static int getEqSol (String line){
		if (line.contains("Non Equivalent Solutions: "))
			return Integer.valueOf(line.split("Non Equivalent Solutions: ")[1].split("\n")[0]);
		return 0;
	}
}
