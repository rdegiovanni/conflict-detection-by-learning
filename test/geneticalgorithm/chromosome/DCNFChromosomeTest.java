package geneticalgorithm.chromosome;

import static org.junit.Assert.*;


import org.jgap.InvalidConfigurationException;
import org.jgap.impl.DefaultConfiguration;
import org.junit.Test;

import gov.nasa.ltl.trans.Formula;
import gov.nasa.ltl.trans.ParseErrorException;

public class DCNFChromosomeTest {

	@Test
	public void test0() throws ParseErrorException, InvalidConfigurationException {
		String f = "[]((p && X(p)) -> X(X(! h)))";
		DCNFChromosome c = new DCNFChromosome(f, new DefaultConfiguration());
		Formula<String> form = c.getLTLFormula();
		System.out.println(form);
	}

}
