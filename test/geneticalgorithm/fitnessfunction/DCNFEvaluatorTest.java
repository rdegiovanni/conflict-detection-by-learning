package geneticalgorithm.fitnessfunction;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.jgap.InvalidConfigurationException;
import org.jgap.impl.DefaultConfiguration;
import org.junit.Test;

import geneticalgorithm.chromosome.DCNFChromosome;
import gov.nasa.ltl.trans.Formula;
import gov.nasa.ltl.trans.ParseErrorException;
import gov.nasa.ltl.trans.Parser;

public class DCNFEvaluatorTest {

	@Test
	public void testFalse() throws ParseErrorException, InvalidConfigurationException {
		String d = "[]((p && X(p)) -> X(X(! h)))";
		String g1 = "[](h -> X(p))";
		String g2 = "[](m -> X(! p))";
		Set<Formula<String>> dom = new HashSet<>();
		dom.add(Parser.parse(d));
		Set<Formula<String>>goals = new HashSet<>();
		goals.add(Parser.parse(g1));
		goals.add(Parser.parse(g2));
		System.out.println("DOM: "+dom);
		System.out.println("GOALS: "+goals);
		DCNFEvaluator e = new DCNFEvaluator(dom, goals);
		DCNFChromosome c = new DCNFChromosome("false", new DefaultConfiguration());
		double f = e.evaluate(c);
		assertTrue(f==(double)0);
	}
	
	@Test
	public void testTrue() throws ParseErrorException, InvalidConfigurationException {
		String d = "[]((p && X(p)) -> X(X(! h)))";
		String g1 = "[](h -> X(p))";
		String g2 = "[](m -> X(! p))";
		Set<Formula<String>> dom = new HashSet<>();
		dom.add(Parser.parse(d));
		Set<Formula<String>>goals = new HashSet<>();
		goals.add(Parser.parse(g1));
		goals.add(Parser.parse(g2));
		System.out.println("DOM: "+dom);
		System.out.println("GOALS: "+goals);
		DCNFEvaluator e = new DCNFEvaluator(dom, goals);
		DCNFChromosome c = new DCNFChromosome("true", new DefaultConfiguration());
		double f = e.evaluate(c);
		assertTrue(f==(double)0);
	}
	
	@Test
	public void testMinePump() throws ParseErrorException, InvalidConfigurationException {
		String d = "[]((p && X(p)) -> X(X(! h)))";
		String g1 = "[](h -> X(p))";
		String g2 = "[](m -> X(! p))";
		Set<Formula<String>> dom = new HashSet<>();
		dom.add(Parser.parse(d));
		Set<Formula<String>>goals = new HashSet<>();
		goals.add(Parser.parse(g1));
		goals.add(Parser.parse(g2));
		System.out.println("DOM: "+dom);
		System.out.println("GOALS: "+goals);
		DCNFEvaluator e = new DCNFEvaluator(dom, goals);
		DCNFChromosome c = new DCNFChromosome("<>(h && m)", new DefaultConfiguration());
		double f = e.evaluate(c);
		System.out.println();
		System.out.println(f);
		assertTrue(f>=DCNFEvaluator.MIN_SOLUTION_VALUE);
	}
	
	@Test
	public void testRRCS() throws ParseErrorException, InvalidConfigurationException {
		String d1 = "[] (ta <-> X(tc))";
		String d2 = "[] (X(cc) -> ca && go)";
		String g1 = "[] (tc -> !cc)";
		String g2 = "[] (ta -> !go)";
		
		Set<Formula<String>> dom = new HashSet<>();
		dom.add(Parser.parse(d1));
		dom.add(Parser.parse(d2));
//		dom.add(Parser.parse("!ta && !ca"));
		Set<Formula<String>>goals = new HashSet<>();
		goals.add(Parser.parse(g1));
		goals.add(Parser.parse(g2));
		DCNFEvaluator e = new DCNFEvaluator(dom, goals);
		DCNFChromosome c = new DCNFChromosome("( ( ( go ) && ( tc ) ) W ( ( cc ) && ( tc ) ) )", new DefaultConfiguration());
		double f = e.evaluate(c);
		System.out.println();
		System.out.println(f);
		assertTrue(f>=DCNFEvaluator.MIN_SOLUTION_VALUE);
	}
	
	@Test
	public void testELevator() throws ParseErrorException, InvalidConfigurationException {
		String d = "[] ( X(open) -> atfloor)";
		String g1 = "[](call -> <>(open))";
		Set<Formula<String>> dom = new HashSet<>();
		dom.add(Parser.parse(d));
		Set<Formula<String>>goals = new HashSet<>();
		goals.add(Parser.parse(g1));
		System.out.println("DOM: "+dom);
		System.out.println("GOALS: "+goals);
		DCNFEvaluator e = new DCNFEvaluator(dom, goals);
		DCNFChromosome c = new DCNFChromosome("X(<>(call && [](! open)))", new DefaultConfiguration());
		double f = e.evaluate(c);
		System.out.println();
		System.out.println(f);
		assertTrue(f > DCNFEvaluator.MIN_SOLUTION_VALUE);
	}
	
	@Test
	public void testTelephone() throws ParseErrorException, InvalidConfigurationException {
		String d1 = "[](o -> ! f)";
		String d2 = "[](o -> ! c)";
		String d3 = "[](c -> ! f)";
		String g1 = "[](c  -> (c  U (o || d)) )";
		String g2 = "[](c  ->  (c  U (f || d)) )";
		Set<Formula<String>> dom = new HashSet<>();
		dom.add(Parser.parse(d1));
		dom.add(Parser.parse(d2));
		dom.add(Parser.parse(d3));
		Set<Formula<String>>goals = new HashSet<>();
		goals.add(Parser.parse(g1));
		goals.add(Parser.parse(g2));
		System.out.println("DOM: "+dom);
		System.out.println("GOALS: "+goals);
		DCNFEvaluator e = new DCNFEvaluator(dom, goals);
		DCNFChromosome c = new DCNFChromosome("<> (((c && (! o && (! d && ! f))) && X (((! c && ((! o && ! d) || (o && (! d || f)))) || (c && (o || f))))))", new DefaultConfiguration());
		double f = e.evaluate(c);
		System.out.println();
		System.out.println(f);
		assertTrue(f > DCNFEvaluator.MIN_SOLUTION_VALUE);
	}
	
	@Test
	public void testLiftControllerTwoFloors() throws ParseErrorException, InvalidConfigurationException {
	//Counter-Strategy Guided Refinement of GR(1) Temporal Logic Specifications
		String d1 = "!b1 && !b2";
		String d2 = "[]((b1 && f1) -> X(!b1))";
		String d3 = "[]((b2 && f2) -> X(!b2))";
		String d5 = "[]((b1 && !f1) -> X(b1))";
		String d6 = "[]((b2 && !f2) -> X(b2))";
		String g1 = "f1 && !f2";
		String g2 = "[](!(f1 && f2))";
		String g3 = "[](f1 -> X (f1 || f2))";
		String g4 = "[](f2 -> X (f1 || f2))";
		String g6 = "[]((f1 && X(f2)) -> X (b1 || b2))";
		String g7 = "[]<>(b1 -> f1)";
		String g8 = "[]<>(b2 -> f2)";
		String g10 = "[]<>(f1)";
		String g11 = "[]<>(f2)";
		
			Set<Formula<String>> dom = new HashSet<>();
			dom.add(Parser.parse(d1));
			dom.add(Parser.parse(d2));
			dom.add(Parser.parse(d3));
			dom.add(Parser.parse(d5));
			dom.add(Parser.parse(d6));
			Set<Formula<String>> goals = new HashSet<>();
			goals.add(Parser.parse(g1));
			goals.add(Parser.parse(g2));
			goals.add(Parser.parse(g3));
			goals.add(Parser.parse(g4));
			goals.add(Parser.parse(g6));
			goals.add(Parser.parse(g7));
			goals.add(Parser.parse(g8));
			goals.add(Parser.parse(g10));
			goals.add(Parser.parse(g11));
			
			DCNFEvaluator e = new DCNFEvaluator(dom, goals);
//			DCNFChromosome c = new DCNFChromosome("<>[](!b1 && !b2)", new DefaultConfiguration());
			DCNFChromosome c = new DCNFChromosome("( ( ( <> ( ( false ) W ( ( b1 ) && ( ! ( f2 ) ) ) ) ) U ( ( ! ( b1 ) ) && ( [] ( ! ( f1 ) ) ) ) ) W ( <> ( ( f2 ) W ( f2 ) ) ) )", new DefaultConfiguration());
			double f = e.evaluate(c);
			System.out.println();
			System.out.println(f);
			assertTrue(f > DCNFEvaluator.MIN_SOLUTION_VALUE);
	}

	@Test
	public void testSimpleArbitrer() throws ParseErrorException, InvalidConfigurationException {
	//Counter-Strategy Guided Refinement of GR(1) Temporal Logic Specifications
		String d0 = "!r1";
		String d1 = "!r2";
		String d2 = "[]((((!r1)&&(g1))||((r1)&&(!g1)))->(((!r1)&&((X(!r1))))||((r1)&&((X(r1))))))";
		String d3 = "[]((((!r2)&&(g2))||((r2)&&(!g2)))->(((!r2)&&((X(!r2))))||((r2)&&((X(r2))))))";
		String d4 = "[](<>(!((r1)&&(g1))))";
		String d5 = "[](<>(!((r2)&&(g2))))";
		String g0 = "!g1";
		String g1 = "!g2";
		String g2 = "[](!(((X(g1)))&&((X(g2)))))";
		String g3 = "[]((((!r1)&&(!g1))||((r1)&&(g1)))->(((!g1)&&((X(!g1))))||((g1)&&((X(g1))))))";
		String g4 = "[]((((!r2)&&(!g2))||((r2)&&(g2)))->(((!g2)&&((X(!g2))))||((g2)&&((X(g2))))))";
		String g5 = "[](<>(((!r1)&&(!g1))||((r1)&&(g1))))";
		String g6 = "[](<>(((!r2)&&(!g2))||((r2)&&(g2))))";
		
			Set<Formula<String>> dom = new HashSet<>();
			dom.add(Parser.parse(d0));
			dom.add(Parser.parse(d1));
			dom.add(Parser.parse(d2));
			dom.add(Parser.parse(d3));
			dom.add(Parser.parse(d4));
			dom.add(Parser.parse(d5));
			
			Set<Formula<String>> goals = new HashSet<>();
			goals.add(Parser.parse(g0));
			goals.add(Parser.parse(g1));
			goals.add(Parser.parse(g2));
			goals.add(Parser.parse(g3));
			goals.add(Parser.parse(g4));
			goals.add(Parser.parse(g5));
			goals.add(Parser.parse(g6));
			
			DCNFEvaluator e = new DCNFEvaluator(dom, goals);
			DCNFChromosome c = new DCNFChromosome("( ( ( X ( g2 ) ) || ( ( r1 ) W ( g1 ) ) ) || ( <> ( [] ( ( false ) || ( ( r1 ) || ( g2 ) ) ) ) ) ) ", new DefaultConfiguration());
			double f = e.evaluate(c);
			System.out.println();
			System.out.println(f);
			assertTrue(f > DCNFEvaluator.MIN_SOLUTION_VALUE);
	}
	
}
