package dCNF;

import static org.junit.Assert.*;



import org.junit.Test;

import gov.nasa.ltl.trans.Formula;
import gov.nasa.ltl.trans.ParseErrorException;
import gov.nasa.ltl.trans.Parser;

public class LTLParserTest {


	@Test
	public void test1() throws ParseErrorException  {
		String formula = "p && r <-> q";
		Formula<String> f =Parser.parse(formula);
		
		System.out.println(f);
	}

	@Test
	public void testRelease() throws ParseErrorException  {
		String formula = "p R q";
		Formula<String> f =Parser.parse(formula);

		System.out.println(f);
	}
	
	@Test
	public void test2() throws ParseErrorException  {
		String formula = "!(! p && q)";
		LTL2dCNF trans = new LTL2dCNF(formula);
		Formula<String> f = trans.toPNF();
		System.out.println(f);
	}
	
	@Test
	public void test3() throws ParseErrorException  {
		String formula = "((!(p U !(r U q))))";
		LTL2dCNF trans = new LTL2dCNF(formula);
		System.out.println(trans.ltl);
		Formula<String> f = trans.toPNF();
		System.out.println(f);
	}
	
	@Test
	public void test4() throws ParseErrorException  {
		String formula = "p && q";
		LTL2dCNF trans = new LTL2dCNF(formula);
		System.out.println(trans.ltl);
		trans.toDCNF();
		System.out.println(trans.dcnf);
	}
	
	@Test
	public void test5a() throws ParseErrorException  {
		String formula = "[](X (p && q))";
		LTL2dCNF trans = new LTL2dCNF(formula);
		System.out.println(trans.ltl);
		trans.toDCNF();
		System.out.println(trans.dcnf);
	}
	
	@Test
	public void test5b() throws ParseErrorException  {
		String formula = "(p && q) || (r && s)";
		LTL2dCNF trans = new LTL2dCNF(formula);
		System.out.println(trans.ltl);
		trans.toDCNF();
		System.out.println(trans.dcnf);
	}
	
	@Test
	public void test6() throws ParseErrorException  {
		String formula = "<>(m && !p)";
		LTL2dCNF trans = new LTL2dCNF(formula);
		System.out.println(trans.ltl);
		trans.toDCNF();
		System.out.println(trans.dcnf);
		System.out.println(trans.dCNF());
		
	}
	
	@Test
	public void test7() throws ParseErrorException  {
		String formula = "[](p && q)";
		Formula<String> f =Parser.parse(formula);
		System.out.println(f);
		System.out.println(f.toPLTLString());
	}
	
	@Test
	public void test8() throws ParseErrorException  {
		String formula = "[](p && q)";
		Formula<String> f =Parser.parse(formula);
		Formula<String> p = Formula.And(Formula.Proposition("p"), Formula.Proposition("r"));
		Formula<String> r = Formula.Proposition("r");
		System.out.println(f);
		System.out.println(f.replaceSubFormula(p, r));
	}
	@Test
	public void test9() throws ParseErrorException  {
		String formula = "<>(m) && <>(h)";
		LTL2dCNF f = new LTL2dCNF(formula);
		
		f.dCNF();
		System.out.println(f.dcnf);
	}

	@Test
	public void test10() throws ParseErrorException  {
		String formula = "[](p && q) || p";
		LTL2dCNF f = new LTL2dCNF(formula);

		f.dCNF();
		System.out.println(f.dcnf);
	}

	@Test
	public void test11() throws ParseErrorException  {
		String formula = "( ( p0 ) /\\ ( ( ( ! ( p0 ) ) W ( X ( ( ( p0 ) \\/ ( p2 ) ) V ( ( p0 ) \\/ ( p2 ) ) ) ) ) /\\ ( ! ( p1 ) ) ) )";
		LTL2dCNF f = new LTL2dCNF(formula);
		f.dCNF();
		System.out.println(f.dcnf);
		Formula<String> p = Parser.parse("p0");
		Formula<String> r = Parser.parse("( X ( ( p0 ) /\\ ( ( ( p1 ) \\/ ( ( p2 ) \\/ ( <> ( ( ( p0 ) W ( p1 ) ) V ( ( p0 ) W ( p1 ) ) ) ) ) ) /\\ ( ! ( p1 ) ) ) ) )");
		Formula<String> res = f.ltl.replaceSubFormula(p,r);
		System.out.println(res);
	}
	
}