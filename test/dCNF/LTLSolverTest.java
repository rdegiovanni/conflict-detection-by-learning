package dCNF;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

import dCNF.LTLSolver.SolverResult;
import gov.nasa.ltl.trans.Formula;
import gov.nasa.ltl.trans.ParseErrorException;
import gov.nasa.ltl.trans.Parser;

public class LTLSolverTest {

	@Test
	public void test1() throws IOException, InterruptedException {
		String formula = "G (p & q) & F(~ p)";
		SolverResult sat = LTLSolver.isSAT(formula);
		assertTrue("expected unsat",sat==SolverResult.UNSAT);
	}
	
	@Test
	public void test2() throws IOException, InterruptedException {
		String formula = "G (p & q)";
		SolverResult sat = LTLSolver.isSAT(formula);
		assertTrue("expected sat",sat==SolverResult.SAT);
	}
	
	@Test
	public void test3() throws ParseErrorException, IOException, InterruptedException  {
		String formula = "[](p && q)";
		Formula<String> f =Parser.parse(formula);
		System.out.println(f);
		String g = f.toPLTLString();
		System.out.println(g);
		SolverResult sat = LTLSolver.isSAT(g);
		assertTrue("expected sat",sat==SolverResult.SAT);
	}
	
	@Test
	public void test4() throws ParseErrorException, IOException, InterruptedException  {
		String formula = "![](!p && q) && [](!p && q)";
		Formula<String> f =Parser.parse(formula);
		System.out.println(f);
		String g = f.toPLTLString();
		System.out.println(g);
		SolverResult sat = LTLSolver.isSAT(g);
		assertTrue("expected unsat",sat==SolverResult.UNSAT);
	}
	
	@Test
	public void test5() throws ParseErrorException, IOException, InterruptedException  {
		String formula = "false";
		Formula<String> f =Parser.parse(formula);
		System.out.println(f);
		String g = f.toPLTLString();
		System.out.println(g);
		SolverResult sat = LTLSolver.isSAT(g);
		assertTrue("expected unsat",sat==SolverResult.UNSAT);
	}
	
	@Test
	public void test6() throws ParseErrorException, IOException, InterruptedException  {
		String formula = "true";
		Formula<String> f =Parser.parse(formula);
		System.out.println(f);
		String g = f.toPLTLString();
		System.out.println(g);
		SolverResult sat = LTLSolver.isSAT(g);
		assertTrue("expected sat",sat==SolverResult.SAT);
	}
}
